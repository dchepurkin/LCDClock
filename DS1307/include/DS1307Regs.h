#pragma once

const uint8_t DS1307_ADDRESS = 0x68;

const uint8_t SECONDS_REG = 0x00;
const uint8_t MINUTES_REG = 0x01;
const uint8_t HOURS_REG = 0x02;

const uint8_t DAY_OF_WEEK_REG = 0x03;
const uint8_t DAY_REG = 0x04;
const uint8_t MONTH_REG = 0x05;
const uint8_t YEAR_REG = 0x06;