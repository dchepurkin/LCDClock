#pragma once

#include <pico/stdlib.h>
#include <string>

struct DTime
{
	DTime() = default;

	DTime(const uint8_t InHours, const uint8_t InMinutes, const uint8_t InSeconds = 0)
			: Hours(InHours),
			  Minutes(InMinutes),
			  Seconds(InSeconds) {}

	DTime(const DTime& Other) = default;

	DTime& operator=(const DTime& Other)
	{
		Seconds = Other.Seconds;
		if(Minutes != Other.Minutes)
		{
			Minutes = Other.Minutes;
			bIsMinutesChanged = true;
		}

		if(Hours != Other.Hours)
		{
			Hours = Other.Hours;
			bIsHoursChanged = true;
		}

		return *this;
	}

	constexpr bool operator==(const DTime& Other) const
	{
		return Hours == Other.Hours && Minutes == Other.Minutes;
	}

	constexpr bool operator<(const DTime& Other) const
	{
		return Hours < Other.Hours || (Hours == Other.Hours && Minutes < Other.Minutes);
	}

	constexpr bool operator>=(const DTime& Other) const
	{
		return Hours >= Other.Hours && Minutes >= Other.Minutes;
	}

	void Set(const uint8_t InHours, const uint8_t InMinutes, const uint8_t InSeconds = 0)
	{
		if(InHours > 23 || InMinutes > 59 || InSeconds > 59) { return; }

		Hours = InHours;
		Minutes = InMinutes;
		Seconds = InSeconds;
	}

	void AddHour(int8_t InValue)
	{
		if(InValue < 0 && -InValue > Hours)
		{
			Hours = 24 + InValue + Hours;
		}
		else
		{
			Hours += InValue;
			if(Hours >= 24)
			{
				Hours %= 24;
			}
		}
	}

	void AddHour()
	{
		if(++Hours == 24)
		{
			Hours = 0;
		}
	}

	void AddMinute(int InValue)
	{
		if(InValue < 0 && -InValue > Minutes)
		{
			Minutes = 60 + InValue + Minutes;
		}
		else
		{
			Minutes += InValue;
			if(Minutes >= 60)
			{
				Minutes %= 60;
			}
		}
	}

	void AddMinute()
	{
		bIsMinutesChanged = true;
		if(++Minutes == 60)
		{
			Minutes = 0;
			AddHour();
		}
	}

	DTime& operator++()
	{
		bIsMinutesChanged = false;
		bIsHoursChanged = false;
		if(++Seconds == 60)
		{
			Seconds = 0;
			AddMinute();
		}

		return *this;
	}

	inline uint8_t GetHighHourNum() const { return Hours / 10; } //����� ����

	inline uint8_t GetLowHourNum() const { return Hours % 10; } //������ ����

	inline uint8_t GetHighMinutesNum() const { return Minutes / 10; }

	inline uint8_t GetLowMinutesNum() const { return Minutes % 10; }

	inline uint8_t GetHighSecondsNum() const { return Seconds / 10; }

	inline uint8_t GetLowSecondsNum() const { return Seconds % 10; }

	inline uint8_t GetSecondsByte() const { return GetDS1307TimeByte(GetHighSecondsNum(), GetLowSecondsNum()); }

	inline uint8_t GetMinutesByte() const { return GetDS1307TimeByte(GetHighMinutesNum(), GetLowMinutesNum()); }

	inline uint8_t GetHoursByte() const { return GetDS1307TimeByte(GetHighHourNum(), GetLowHourNum()); }

	uint8_t Hours = 0;
	uint8_t Minutes = 0;
	uint8_t Seconds = 0;
	bool bIsMinutesChanged = false;
	bool bIsHoursChanged = false;

private:
	inline uint8_t GetDS1307TimeByte(uint8_t InHighNum, uint8_t InLowNum) const
	{
		return (InHighNum << 4) | InLowNum;
	}
};

struct DDate
{
	DDate()
			: DDate(1, 1, 0) {}

	//Year - �� 0 �� 99
	DDate(const uint8_t InDay, const uint8_t InMonth, const uint8_t InYear)
			: Day(InDay),
			  Month(InMonth),
			  Year(InYear)
	{
		UpdateWeekOfDay();
	}

	DDate(const DDate& Other) = default;

	DDate& operator=(const DDate& Other) = default;

	void AddDay(int InValue)
	{
		const auto DaysOfMonth = GetDaysOfMount();
		if(InValue < 0 && -InValue > Day)
		{
			Day = DaysOfMonth + InValue + Day;
		}
		else
		{
			Day += InValue;
			if(Day > DaysOfMonth)
			{
				Day %= DaysOfMonth;
			}
			else if(Day == 0)
			{
				Day = DaysOfMonth;
			}
		}
	}

	void AddMonth(int InValue)
	{
		Month += InValue;
		if(Month > 12)
		{
			Month = 1;
		}
		else if(Month == 0)
		{
			Month = 12;
		}

		const auto DaysOfMonth = GetDaysOfMount();
		if(Day > DaysOfMonth) { Day = DaysOfMonth; }
	}

	void AddYear(int InValue)
	{
		if(InValue < 0 && -InValue > Year)
		{
			Year = 100 + InValue + Year;
		}
		else
		{
			Year += InValue;
			if(Year >= 100)
			{
				Year %= 100;
			}
		}
	}

	DDate& operator++()
	{
		if(++Day > GetDaysOfMount())
		{
			Day = 1;
			if(++Month > 12)
			{
				Month = 1;
				++Year;
			}
		}

		UpdateWeekOfDay();
		return *this;
	}

	constexpr bool operator==(const DDate& Other) const
	{
		return Day == Other.Day && Month == Other.Month && Year == Other.Year;
	}

	constexpr bool operator!=(const DDate& Other) const
	{
		return Day != Other.Day || Month != Other.Month || Year != Other.Year;
	}

	constexpr bool operator<(const DDate& Other) const
	{
		return Year < Other.Year || Month < Other.Month || Day < Other.Day;
	}

	void Set(const uint8_t InDay, const uint8_t InMonth, const uint8_t InYear)
	{
		Day = InDay;
		Month = InMonth;
		Year = InYear;
		UpdateWeekOfDay();
	}

	inline uint8_t GetHighDayNum() const { return Day / 10; } //����� ����

	inline uint8_t GetLowDayNum() const { return Day % 10; } //������ ����

	inline uint8_t GetHighMonthNum() const { return Month / 10; } //����� ����

	inline uint8_t GetLowMonthNum() const { return Month % 10; } //������ ����

	inline uint8_t GetHighYearNum() const { return Year / 10; } //����� ����

	inline uint8_t GetLowYearNum() const { return Year % 10; } //������ ����

	inline uint8_t GetDayByte() const
	{
		return GetDS1307DateByte(GetHighDayNum(), GetLowDayNum());
	}

	inline uint8_t GetMonthByte() const
	{
		return GetDS1307DateByte(GetHighMonthNum(), GetLowMonthNum());
	}

	inline uint8_t GetYearByte() const
	{
		return GetDS1307DateByte(GetHighYearNum(), GetLowYearNum());
	}

	std::string GetMonthName() const
	{
		switch(Month)
		{
			case 1: return "������";
			case 2: return "�������";
			case 3: return "�����";
			case 4: return "������";
			case 5: return "���";
			case 6: return "����";
			case 7: return "����";
			case 8: return "�������";
			case 9: return "��������";
			case 10: return "�������";
			case 11: return "������";
			case 12: return "�������";
		}

		return "������";
	}

	const char* GetWeekOfDayAsString() const
	{
		switch(DayOfWeek)
		{
			case 1: return "�����������";
			case 2: return "�������";
			case 3: return "�����";
			case 4: return "�������";
			case 5: return "�������";
			case 6: return "�������";
			case 7: return "�����������";
		}

		return "������";
	}

	uint8_t Year;
	uint8_t Month;
	uint8_t Day;
	uint8_t DayOfWeek;

private:
	void UpdateWeekOfDay()
	{
		const auto YearCode = (6 + Year + Year / 4) % 7;
		auto Result = (Day + GetMonthCode() + YearCode) % 7;

		if(Year % 4 == 0 && Month <= 2)
		{
			--Result;
		}

		Result = (Result + 6) % 7;

		DayOfWeek = Result == 0 ? 7 : Result;
	}

	uint8_t GetMonthCode() const
	{
		switch(Month)
		{
			case 1: return 1;
			case 2: return 4;
			case 3: return 4;
			case 4: return 0;
			case 5: return 2;
			case 6: return 5;
			case 7: return 0;
			case 8: return 3;
			case 9: return 6;
			case 10: return 1;
			case 11: return 4;
			case 12: return 6;
		}

		return 1;
	}

	inline uint8_t GetDS1307DateByte(uint8_t InHighNum, uint8_t InLowNum) const
	{
		return (InHighNum << 4) | InLowNum;
	}

	uint8_t GetDaysOfMount()
	{
		if(Month == 2)
		{
			return Year % 4 == 0 ? 29 : 28;
		}

		if(Month < 8)
		{
			return Month % 2 == 0 ? 30 : 31;
		}

		return Month % 2 == 0 ? 31 : 30;
	}
};