#pragma once

#include <pico/stdlib.h>
#include <hardware/i2c.h>
#include "DS1307Types.h"

class DS1307
{
public:
	DS1307();

	DS1307(i2c_inst* In2CInst, const uint8_t InSDA, const uint8_t InSCL);

	void SetTime(const DTime& InTime) const;

	void SetDate(const DDate& InDate) const;

	void SetDateTime(const DDate& InDate, const DTime& InTime) const;

	void SynchronizeTime(DTime& OutTime) const;

	void SynchronizeDate(DDate& OutDate) const;

private:
	i2c_inst_t* I2CInst = i2c1;

	const uint SDA = 26;

	const uint SCL = 27;

	void Init();
};