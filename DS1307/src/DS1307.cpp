#ifdef DEBUG
	#include <cstdio>
#endif

#include "DS1307.h"
#include "DS1307Regs.h"

DS1307::DS1307()
{
	Init();
}

DS1307::DS1307(i2c_inst* In2CInst, const uint8_t InSDA, const uint8_t InSCL)
		: I2CInst(In2CInst),
		  SDA(InSDA),
		  SCL(InSCL)
{
	Init();
}

void DS1307::Init()
{
	i2c_init(I2CInst, 57600);
	gpio_set_function(SCL, GPIO_FUNC_I2C);
	gpio_set_function(SDA, GPIO_FUNC_I2C);
}

void DS1307::SynchronizeTime(DTime& OutTime) const
{
#ifdef DEBUG
	printf("Synchronize Time To DS1307. ");
#endif

	uint8_t TimeBuf[3];

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &SECONDS_REG, 1, true);//true ������ ��� ������ ����� ��� �������
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, TimeBuf, 1, false);//false - ���������

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &MINUTES_REG, 1, true);
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, TimeBuf + 1, 1, false);

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &HOURS_REG, 1, true);
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, TimeBuf + 2, 1, false);

	OutTime.Set(10 * ((TimeBuf[2] & 0b00110000) >> 4) + (TimeBuf[2] & 0b00001111),//
			10 * (TimeBuf[1] >> 4) + (TimeBuf[1] & 0b00001111),//
			10 * (TimeBuf[0] >> 4) + (TimeBuf[0] & 0b00001111));//
#ifdef DEBUG
	printf("Time is %02i:%02i\n", OutTime.Hours, OutTime.Minutes);
#endif
}

void DS1307::SetDateTime(const DDate& InDate, const DTime& InTime) const
{
	SetDate(InDate);
	SetTime(InTime);
}

void DS1307::SetTime(const DTime& InTime) const
{
	const uint8_t SecondsBuff[2] = {SECONDS_REG, InTime.GetSecondsByte()};
	const uint8_t MinutesBuff[2] = {MINUTES_REG, InTime.GetMinutesByte()};
	const uint8_t HoursBuff[2] = {HOURS_REG, InTime.GetHoursByte()};

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, SecondsBuff, 2, false);
	i2c_write_blocking(I2CInst, DS1307_ADDRESS, MinutesBuff, 2, false);
	i2c_write_blocking(I2CInst, DS1307_ADDRESS, HoursBuff, 2, false);
}

void DS1307::SetDate(const DDate& InDate) const
{
	const uint8_t DayBuff[2] = {DAY_REG, InDate.GetDayByte()};
	const uint8_t MonthBuff[2] = {MONTH_REG, InDate.GetMonthByte()};
	const uint8_t YearBuff[2] = {YEAR_REG, InDate.GetYearByte()};

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, DayBuff, 2, false);
	i2c_write_blocking(I2CInst, DS1307_ADDRESS, MonthBuff, 2, false);
	i2c_write_blocking(I2CInst, DS1307_ADDRESS, YearBuff, 2, false);
}

void DS1307::SynchronizeDate(DDate& OutDate) const
{
#ifdef DEBUG
	printf("Synchronize Date To DS1307. ");
#endif

	uint8_t DateBuf[3];

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &DAY_REG, 1, true);//true ������ ��� ������ ����� ��� �������
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, DateBuf, 1, false);//false - ���������

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &MONTH_REG, 1, true);
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, DateBuf + 1, 1, false);

	i2c_write_blocking(I2CInst, DS1307_ADDRESS, &YEAR_REG, 1, true);
	i2c_read_blocking(I2CInst, DS1307_ADDRESS, DateBuf + 2, 1, false);

	OutDate.Set(10 * (DateBuf[0] >> 4) + (DateBuf[0] & 0b00001111) //
				, 10 * (DateBuf[1] >> 4) + (DateBuf[1] & 0b00001111) //
				, 10 * (DateBuf[2] >> 4) + (DateBuf[2] & 0b00001111) //
	);//

#ifdef DEBUG
	printf("Today is %02i.%02i.20%02i - %i\n", OutDate.Day, OutDate.Month, OutDate.Year, OutDate.DayOfWeek);
#endif
}






