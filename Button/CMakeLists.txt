cmake_minimum_required(VERSION 3.13)

project(Button)

set(HEADER_FILES include/DButton.h)
set(SOURCE_FILES src/DButton.cpp)

add_library(Button ${SOURCE_FILES} ${HEADER_FILES})

target_link_libraries(Button pico_stdlib Coroutine Delegate)

target_include_directories(Button PUBLIC include/)
target_include_directories(Button PRIVATE src/)

