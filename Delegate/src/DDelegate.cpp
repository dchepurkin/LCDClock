#include "DDelegate.h"

auto DDelegate::FindObserver(Func InFunc)
{
	return std::find_if(Observers.begin(), Observers.end(), [InFunc](std::unique_ptr<DCallback>& InObserver)
	{
		if(const auto TObserver = static_cast<DFunction*>(InObserver.get()))
		{
			return *TObserver == DFunction{InFunc};
		}
		return false;
	});
}

void DDelegate::Bind(Func InCallback)
{
	const auto FoundObserver = FindObserver(InCallback);

	if(FoundObserver == Observers.end())
	{
		Observers.emplace_front(std::make_unique<DFunction>(InCallback));
	}
}

void DDelegate::Unbind(Func InCallback)
{
	const auto FoundObserver = FindObserver(InCallback);

	if(FoundObserver != Observers.end())
	{
		Observers.remove(*FoundObserver);
	}
}
