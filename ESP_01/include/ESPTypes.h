#pragma once

#include <pico/stdlib.h>

enum class EWifiCommands : uint8_t
{
	ConnectStatus, //������ ����������� � ����
	AccessibleWifi, //����� ��������� �����
	Connect, //������������ � ����
	Disconnect, //����������� �� ����
	DateTime, //������ ������� � ����
	NTPConnect,//����������� � ������� ������������� �������
	Reset //������������
};