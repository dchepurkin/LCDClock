#pragma once

#include <string>
#include <pico/stdlib.h>
#include <hardware/uart.h>
#include "DDelegate.h"
#include "ESPTypes.h"
#include "DS1307Types.h"

DECLARE_DELEGATE(OnUARTIrqSignature);
DECLARE_DELEGATE_OneParam(OnWifiConnectedSignature, bool);
DECLARE_DELEGATE(OnSynchronizeFailedSignature);
DECLARE_DELEGATE_OneParam(OnFoundWifiSignature, const std::list<std::string>&);
DECLARE_DELEGATE_TwoParams(OnGetDateTimeSignature, const DDate&, const DTime&);

class ESP_01
{
public:
	OnWifiConnectedSignature OnWifiConnected;
	OnFoundWifiSignature OnFoundWifi;
	OnGetDateTimeSignature OnGetDateTime;
	OnSynchronizeFailedSignature OnSynchronizeFailed;

	ESP_01() = delete;

	ESP_01(const uint InRXPin, const uint InTXPin, uart_inst_t* InUARTInst = uart1);

	void Init();

	bool IsConnected() const { return bIsConnected; }

	template<EWifiCommands>
	void SetCommand();

	template<EWifiCommands>
	void SetCommand(const std::string&, const std::string&);

	void GetDateTime();

	void SetSleepEnabled(bool IsEnabled) const;

private:
	inline static OnUARTIrqSignature OnUARTIrq;

	uart_inst_t* UARTInst;

	uint RXPin;

	uint TXPin;

	bool bIsConnected = false;

	void UARTIrq_Callback();

	void Parse(const std::string& InMessage);

	void SetIsConnected(bool IsConnected);

	uint8_t GetMonthByName(const std::string& InName) const;

	void UARTWrite(const std::string& InCmd);
};