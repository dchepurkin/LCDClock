#ifdef DEBUG
	#include <cstdio>
#endif

#include <list>
#include "ESP_01.h"

void ESP_01::Init()
{
	uart_init(UARTInst, 115200);
	gpio_set_function(RXPin, GPIO_FUNC_UART);
	gpio_set_function(TXPin, GPIO_FUNC_UART);

	irq_set_exclusive_handler(UARTInst == uart0 ? UART0_IRQ : UART1_IRQ, []()
	{
		ESP_01::OnUARTIrq.Broadcast();
	});

	irq_set_enabled(UARTInst == uart0 ? UART0_IRQ : UART1_IRQ, true);
	uart_set_irq_enables(UARTInst, true, false);
	uart_set_format(UARTInst, 8, 1, uart_parity_t::UART_PARITY_NONE);
}

template<>
void ESP_01::SetCommand<EWifiCommands::Connect>() = delete;

template<>
void ESP_01::SetCommand<EWifiCommands::DateTime>()
{
	UARTWrite("AT+CIPSNTPTIME?\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::NTPConnect>()
{
	UARTWrite("AT+CIPSNTPCFG=1,3,\"ntp0.ntp-servers.net\",\"ntp2.ntp-servers.net\",\"ntp3.ntp-servers.net\"\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::Reset>()
{
	UARTWrite("AT+RST\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::ConnectStatus>()
{
	UARTWrite("AT+CIPSTATUS\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::AccessibleWifi>()
{
	UARTWrite("AT+CWLAP\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::Disconnect>()
{
	UARTWrite("AT+CWQAP\r\n");
}

template<>
void ESP_01::SetCommand<EWifiCommands::Connect>(const std::string& InWifiName, const std::string& InPassword)
{
	UARTWrite("AT+CWJAP_DEF=\"" + InWifiName + "\",\"" + InPassword + "\"\r\n");
}

void ESP_01::UARTWrite(const std::string& InCmd)
{
	uart_write_blocking(UARTInst, (uint8_t*)InCmd.c_str(), InCmd.size());
}

ESP_01::ESP_01(const uint InRXPin, const uint InTXPin, uart_inst_t* InUARTInst)
		: RXPin(InRXPin),
		  TXPin(InTXPin),
		  UARTInst(InUARTInst)
{
	ESP_01::OnUARTIrq.Bind(this, &ESP_01::UARTIrq_Callback);
}

void ESP_01::UARTIrq_Callback()
{
	std::string Message;
	while(uart_is_readable_within_us(UARTInst, 1000))
	{
		Message += uart_getc(UARTInst);
	}

	Parse(Message);
	//printf("%s", Message.c_str());
}

void ESP_01::Parse(const std::string& InMessage)
{
	if(InMessage.find("WIFI") != std::string::npos)//
	{
		SetIsConnected(InMessage.find("GOT IP") != std::string::npos);
	}
	else if(InMessage.find("CIPSTATUS") != std::string::npos) //������ �����������
	{
		SetIsConnected(InMessage.find("2") != std::string::npos);
	}
	else if(auto ComandNamePosition = InMessage.find("CWLAP:"); ComandNamePosition != std::string::npos) //��������� ����
	{
		std::list<std::string> WifiNames;
		do
		{
			auto WifiNamePosition = InMessage.find('\"', ComandNamePosition) + 1;
			std::string WifiName;

			for(; InMessage[WifiNamePosition] != '\"'; ++WifiNamePosition)
			{
				WifiName += InMessage[WifiNamePosition];
			}
			WifiNames.push_back(WifiName);

			ComandNamePosition = InMessage.find("+CWLAP:", WifiNamePosition);
		} while(ComandNamePosition != std::string::npos);

		OnFoundWifi.Broadcast(WifiNames);
	}
	else if(InMessage.find("CIPSNTPTIME") != std::string::npos)//�������� ���� � �����
	{
		if((InMessage[50]) != '1')
		{

#ifdef DEBUG
			printf("Synchronize to ESP-01\n");
#endif

			std::string MonthName;

			MonthName += InMessage[34];
			MonthName += InMessage[35];
			MonthName += InMessage[36];

			const uint8_t Month = GetMonthByName(MonthName);
			const uint8_t Day = (InMessage[38] - 48) * 10 + (InMessage[39] - 48);
			const uint8_t Year = (InMessage[52] - 48) * 10 + (InMessage[53] - 48);

			const uint8_t Hours = (InMessage[41] - 48) * 10 + (InMessage[42] - 48);
			const uint8_t Minutes = (InMessage[44] - 48) * 10 + (InMessage[45] - 48);
			const uint8_t Seconds = (InMessage[47] - 48) * 10 + (InMessage[48] - 48);

			const DTime NewTime{Hours, Minutes, Seconds};
			const DDate NewDate{Day, Month, Year};

			OnGetDateTime.Broadcast(NewDate, NewTime);
		}
		else
		{
#ifdef DEBUG
			printf("Failed synchronize to ESP-01\n");
#endif
			OnSynchronizeFailed.Broadcast();
		}
	}
}

void ESP_01::SetIsConnected(bool IsConnected)
{
	if(IsConnected != bIsConnected)
	{
		bIsConnected = IsConnected;
		OnWifiConnected.Broadcast(bIsConnected);
	}
}

void ESP_01::GetDateTime()
{
	if(!bIsConnected) { return; }
	SetCommand<EWifiCommands::DateTime>();
}

uint8_t ESP_01::GetMonthByName(const std::string& InName) const
{
	if(InName == "Jan") { return 1; }
	if(InName == "Feb") { return 2; }
	if(InName == "Mar") { return 3; }
	if(InName == "Apr") { return 4; }
	if(InName == "May") { return 5; }
	if(InName == "Jun") { return 6; }
	if(InName == "Jul") { return 7; }
	if(InName == "Aug") { return 8; }
	if(InName == "Sep") { return 9; }
	if(InName == "Oct") { return 10; }
	if(InName == "Nov") { return 11; }
	if(InName == "Dec") { return 12; }
	return 0;
}

void ESP_01::SetSleepEnabled(bool IsEnabled) const
{
	uart_set_irq_enables(UARTInst, !IsEnabled, false);
}




