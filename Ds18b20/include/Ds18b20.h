#pragma once

#include <pico/stdlib.h>

class Ds18b20
{
public:
	explicit Ds18b20(uint8_t InPin);

	void UpdateTemperature();

	int8_t GetTemperature() const { return Temp; }

private:
	uint8_t Pin;

	int8_t Temp = 0;

	void Reset() const;

	void WriteByte(uint8_t InByte);

	void WriteBit(bool InBit) const;

	uint8_t ReadByte();

	bool ReadBit() const;
};