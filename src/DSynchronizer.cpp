#include "DSynchronizer.h"

DSynchronizer::DSynchronizer(DDate& InDate, DTime& InTime)
		: Date(InDate),
		  Time(InTime)
{
	ESP.OnWifiConnected.Bind(this, &DSynchronizer::WifiConnected_Callback);
	ESP.OnGetDateTime.Bind(this, &DSynchronizer::GetDateTime_Callback);
	ESP.OnSynchronizeFailed.Bind(this, &DSynchronizer::WifiSyncFailed_Callback);
	ESPInit_Task();
}

void DSynchronizer::Synchronize()
{
	if(bIsSleeping || !ESP.IsConnected())
	{
		Ds1307Synchronize();
	}
	else
	{
		SyncDateTime_Task();
	}
}

void DSynchronizer::WifiConnected_Callback(bool IsConnected)
{
#ifdef DEBUG
	printf("WIFI %s\n", IsConnected ? "Connected" : "Disconnected");
#endif

	if(IsConnected)
	{
		SyncDateTime_Task();
	}
}

DTask<StartRightNow> DSynchronizer::SyncDateTime_Task()
{
	ESP.SetCommand<EWifiCommands::NTPConnect>();
	co_await DelayMillis(500);
	ESP.SetCommand<EWifiCommands::DateTime>();
}

void DSynchronizer::GetDateTime_Callback(const DDate& InDate, const DTime& InTime)
{
	Ds1307.SetDateTime(InDate, InTime);

	Time = InTime;
	OnTimeSynchronized.Broadcast();

	if(Date != InDate)
	{
		Date = InDate;
		OnDateSynchronized.Broadcast();
	}
}

void DSynchronizer::Ds1307Synchronize()
{
	Ds1307.SynchronizeTime(Time);
	OnTimeSynchronized.Broadcast();

	Ds1307.SynchronizeDate(Date);
	OnDateSynchronized.Broadcast();
}

DTask<StartRightNow> DSynchronizer::ESPInit_Task()
{
	co_await DelayMillis(500);
	ESP.Init();
	co_await DelayMillis(500);
	ESP.SetCommand<EWifiCommands::Reset>();

	//��� ������ �������� �����������������
	//co_await DelayMillis(2000);
	//ESP.SetCommand<EWifiCommands::Connect>("Ananas", "chip666gyg");
}

void DSynchronizer::SetTime(const DTime& InTime) const
{
	Ds1307.SetTime(InTime);
}

void DSynchronizer::SetDate(const DDate& InDate) const
{
	Ds1307.SetDate(InDate);
}

void DSynchronizer::WifiSyncFailed_Callback()
{
	Ds1307Synchronize();
	ResyncWifi();
}

DTask<StartRightNow> DSynchronizer::ResyncWifi()
{
	co_await DelayMillis(1000);
	ESP.GetDateTime();
}






