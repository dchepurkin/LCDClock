#include <cstdio>
#include "DClock.h"
#include "Numbers25_43.h"
#include "AlarmImage.h"
#include "ColonImage.h"
#include "DColors.h"
#include "WifiIcon.h"
#include "SettingsImage.h"
#include "CheckIcon.h"
#include "TempIcon.h"
#include "pico/multicore.h"

template<>
void DClock::SetState<EClockState::Idle>()
{
#ifdef DEBUG
	printf("Set State to IDLE\n");
#endif
	Synchronizer.Wake();
	Synchronizer.Synchronize();
	Buzzer.Stop();
	State = EClockState::Idle;
	Screen.FillScreen(BLACK);
	Screen.SetLedEnabled(true);
	DrawWifiIcon(Synchronizer.IsWifiConnected());
	DrawAlarmIcon(Alarm.IsActive());
	DrawTempIcon();

	TimeTick_Task();
}

template<>
void DClock::SetState<EClockState::Settings>()
{
#ifdef DEBUG
	printf("Set State to SETTINGS\n");
#endif

	State = EClockState::Settings;
	Synchronizer.Sleep();
	SettingsState = ESettingsState::ChooseSettings;
	FocusedSetting = ESettingsState::TimeSettings;
	Screen.DrawBitmap({}, {128, 128}, SettingsImage);
	SwitchFocusedSetting(ESwitchDirection::Left);
}

template<>
void DClock::SetState<EClockState::Alarm>()
{
#ifdef DEBUG
	printf("Set State to ALARM\n");
#endif

	State = EClockState::Alarm;
	Synchronizer.Sleep();
	Screen.DrawBitmap({}, {128, 128}, AlarmImage);
	Screen.SetLedEnabled(true);
	Buzzer.PlaySong(Song, true);
}

void DClock::Begin()
{
	//��� ������ �������� �����������������, ����� ������� ������ ��� � ���������������� �������
	//��� ����� ��� ������� ������� ���� ������ ��� ����������
	//flash_range_erase(ALARM_INFO_OFFSET, FLASH_SECTOR_SIZE);
	//flash_range_erase(ALARM_SONG_OFFSET, FLASH_SECTOR_SIZE);
	//flash_range_program(ALARM_SONG_OFFSET, &FocusedSongIndex, FLASH_PAGE_SIZE);

	UpButton.SetHoldTickDelta(500);
	DownButton.SetHoldTickDelta(500);

	LeftButton.OnPressed.Bind(this, &DClock::LeftButtonPressed_Callback);
	CenterButton.OnPressed.Bind(this, &DClock::CenterButtonPressed_Callback);
	RightButton.OnPressed.Bind(this, &DClock::RightButtonPressed_Callback);
	UpButton.OnPressed.Bind(this, &DClock::UpButtonPressed_Callback);
	DownButton.OnPressed.Bind(this, &DClock::DownButtonPressed_Callback);

	UpButton.OnHoldDown.Bind(this, &DClock::UpButtonHoldDown_Callback);
	DownButton.OnHoldDown.Bind(this, &DClock::DownButtonHoldDown_Callback);

	Synchronizer.BindOnWifiStatusChanged(this, &DClock::DrawWifiIcon);
	Synchronizer.OnDateSynchronized.Bind(this, &DClock::DateSynchronized_Callback);
	Synchronizer.OnTimeSynchronized.Bind(this, &DClock::TimeSynchronized_Callback);

	Screen.SetRotation(ERotation::Degree90);

	Alarm.OnAlarmTriggered.Bind(this, &DClock::AlarmTriggered_Callback);

	TopButton.SetHoldTickDelta(0);
	TopButton.OnPressed.Bind(this, &DClock::TopButtonPressed_Callback);
	TopButton.OnHoldDown.Bind(this, &DClock::TopButtonHoldDown_Callback);

	//������ �� ���� ������ ������ ������������� �������
	Song = Songs[*reinterpret_cast<uint8_t*>(XIP_BASE + ALARM_SONG_OFFSET)];

	SetState<EClockState::Idle>();

	while(true)
	{
		TopButton.Tick();
		if(State == EClockState::Settings)
		{
			LeftButton.Tick();
			CenterButton.Tick();
			RightButton.Tick();
			UpButton.Tick();
			DownButton.Tick();
		}
	}
}

void DClock::SetSong(const DSong& InSong)
{
	Song = InSong;
}

DTask<StartRightNow> DClock::TimeTick_Task()
{
	ShowTime(Time);
	ShowDate(Date);
	ShowDayOfWeek(Date);
	ColonTick_Task();

	while(State == EClockState::Idle)
	{
		co_await DelayMillis(1000);
		++Time;

		if(State != EClockState::Idle) { break; }

		ShowTemp();

		if(Time.Minutes % 15 == 0 && Time.Seconds == 30)
		{
			Synchronizer.Synchronize();
		}

		if(Time.bIsMinutesChanged)
		{

			if(Time.bIsHoursChanged && Time.Hours == 0)
			{
				++Date;
				ShowDate(Date);
				ShowDayOfWeek(Date);
			}

			if(Time >= SleepTime || Time < WakeTime)
			{
				Screen.SetLedEnabled(false);
			}
			else if(Time >= WakeTime)
			{
				Screen.SetLedEnabled(true);
			}

			ShowTime(Time);
			Alarm.Check(Date, Time);
		}
	}
}

void DClock::TopButtonPressed_Callback()
{
	if(Buzzer.IsPlaying()) { Buzzer.Stop(); }

	if(State == EClockState::Idle)
	{
		Screen.SetLedEnabled(true);
	}
	else if(State == EClockState::Alarm)
	{
		SetState<EClockState::Idle>();
	}
	else if(State == EClockState::Settings)
	{
		if(SettingsState == ESettingsState::ChooseSettings)
		{
			SetState<EClockState::Idle>();
		}
		else
		{
			SetState<EClockState::Settings>();
		}
	}
}

void DClock::TopButtonHoldDown_Callback()
{
	SetState<EClockState::Settings>();
}

void DClock::AlarmTriggered_Callback()
{
	SetState<EClockState::Alarm>();
}

void DClock::ShowTime(const DTime& InTime) const
{
	constexpr int YPosition = 42;
	const DVector2D NumberSize{25, 43};

	Screen.DrawBitmap({0, YPosition}, NumberSize, Numbers25_43 + InTime.GetHighHourNum() * Number25_43_BuffSize);
	Screen.DrawBitmap({30, YPosition}, NumberSize, Numbers25_43 + InTime.GetLowHourNum() * Number25_43_BuffSize);
	Screen.DrawBitmap({72, YPosition}, NumberSize, Numbers25_43 + InTime.GetHighMinutesNum() * Number25_43_BuffSize);
	Screen.DrawBitmap({102, YPosition}, NumberSize, Numbers25_43 + InTime.GetLowMinutesNum() * Number25_43_BuffSize);
}

void DClock::ShowDate(const DDate& InDate)
{
	Screen.DrawFillRect({0, 93}, {128, 11}, BLACK);
	auto Year = std::to_string(InDate.Year);
	if(Year.size() == 1)
	{
		Year = "0" + Year;
	}
	std::string DateToShow = std::to_string(InDate.Day) + " " + InDate.GetMonthName() + " 20" + Year;
	Screen.PrintString(DateToShow.c_str(), 93, ETextAlign::Right);
}

void DClock::ShowDayOfWeek(const DDate& InDate)
{
	Screen.DrawFillRect({0, 108}, {128, 11}, BLACK);
	Screen.PrintString(InDate.GetWeekOfDayAsString(), 108, ETextAlign::Right);
}

DTask<StartRightNow> DClock::ColonTick_Task()
{
	bool bIsColonOn = false;

	while(State == EClockState::Idle)
	{
		bIsColonOn = !bIsColonOn;
		DrawColon(bIsColonOn);
		co_await DelayMillis(500);
	}
}

void DClock::DrawColon(bool IsColonOn)
{
	const DVector2D ColonPosition{60, 51};
	const DVector2D ColonSize{7, 27};
	Screen.DrawBitmap(ColonPosition, ColonSize, ColonImage + (IsColonOn ? ColonBuffSize : 0));
}

void DClock::DrawWifiIcon(bool IsConnected)
{
	if(State != EClockState::Idle) { return; }
	Screen.DrawBitmap({112, 0}, {15, 15}, IsConnected ? WifiIcon + WifiIconBuffSize : WifiIcon);
}

void DClock::DrawAlarmIcon(bool IsActive)
{
	if(State != EClockState::Idle) { return; }
	Screen.DrawBitmap({92, 0}, {15, 15}, IsActive ? AlarmIcon + AlarmIconBuffSize : AlarmIcon);
}

void DClock::FoundWifi_Callback(const std::list<std::string>& InWifiNames)
{
	for(const auto& Name : InWifiNames)
	{
		printf("%s\n", Name.c_str());
	}
}

void DClock::DateSynchronized_Callback()
{
	if(State == EClockState::Idle)
	{
		ShowDate(Date);
		ShowDayOfWeek(Date);
	}
}

void DClock::TimeSynchronized_Callback()
{
	if(Time.bIsMinutesChanged && State == EClockState::Idle)
	{
		ShowTime(Time);
		Alarm.Check(Date, Time);
	}
}

void DClock::LeftButtonPressed_Callback()
{
	if(SettingsState == ESettingsState::ChooseSettings)
	{
		SwitchFocusedSetting(ESwitchDirection::Left);
	}
	else if(SettingsState == ESettingsState::TimeSettings)
	{
		auto Position = static_cast<uint8_t>(TimeSettingsState) - 1;
		if(Position < 0) { Position = static_cast<uint8_t>(ETimeSettingsState::YearSetup); }

		TimeSettingsState = static_cast<ETimeSettingsState>(Position);
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
		{
			Alarm.Remove(AlarmCursorPosition + AlarmsOffset);
			AlarmCursorPosition = 0;
			AlarmsOffset = 0;
			ShowAlarmTimeSettings();
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			auto Position = static_cast<uint8_t>(NewAlarmState) - 1;
			if(Position < 0) { Position = 2; }
			NewAlarmState = static_cast<ENewAlarmState>(Position);
		}
	}
}

void DClock::CenterButtonPressed_Callback()
{
	if(SettingsState == ESettingsState::ChooseSettings)
	{
		if(FocusedSetting == ESettingsState::TimeSettings)
		{
			Synchronizer.Synchronize();
			SetupTime = Time;
			SetupDate = Date;
			SettingsState = ESettingsState::TimeSettings;
			TimeSettingsState = ETimeSettingsState::HourSetup;
			TimeSetupBlinkTask();
		}
		else if(FocusedSetting == ESettingsState::AlarmSettings)
		{
			SettingsState = ESettingsState::AlarmSettings;
			if(AlarmSettingsState == EAlarmSettingsState::MelodySettings)
			{
				FocusedSongIndex = 0;
				ShowMelodySettings();
			}
			else if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
			{
				AlarmsOffset = 0;
				AlarmCursorPosition = 0;
				ShowAlarmTimeSettings();
			}
		}
	}
	else if(SettingsState == ESettingsState::TimeSettings)
	{
		SetupTime.Seconds = 0;
		Synchronizer.SetTime(SetupTime);
		Synchronizer.SetDate(SetupDate);
		SetState<EClockState::Settings>();
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::MelodySettings)
		{
			Song = Songs[FocusedSongIndex];

			flash_range_erase(ALARM_SONG_OFFSET, FLASH_SECTOR_SIZE);
			flash_range_program(ALARM_SONG_OFFSET, &FocusedSongIndex, FLASH_PAGE_SIZE);

			Buzzer.Stop();
			SetState<EClockState::Settings>();
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			Alarm.Add(NewAlarm);
			SetState<EClockState::Settings>();
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
		{
			Alarm.ToggleAlarm(AlarmCursorPosition + AlarmsOffset);
			ShowAlarmTimeSettings();
		}
	}
}

void DClock::RightButtonPressed_Callback()
{
	if(SettingsState == ESettingsState::ChooseSettings)
	{
		SwitchFocusedSetting(ESwitchDirection::Right);
	}
	else if(SettingsState == ESettingsState::TimeSettings)
	{
		auto Position = (static_cast<uint8_t>(TimeSettingsState) + 1) % 5;
		TimeSettingsState = static_cast<ETimeSettingsState>(Position);
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
		{
			ShowAddNewAlarmTimeScreen();
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			auto Position = (static_cast<uint8_t>(NewAlarmState) + 1) % 3;
			NewAlarmState = static_cast<ENewAlarmState>(Position);
		}
	}
}

void DClock::UpButtonPressed_Callback()
{
	if(SettingsState == ESettingsState::TimeSettings)
	{
		switch(TimeSettingsState)
		{
			case ETimeSettingsState::HourSetup:
			{
				SetupTime.AddHour(1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::MinutesSetup:
			{
				SetupTime.AddMinute(1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::DaySetup:
			{
				SetupDate.AddDay(1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::MonthSetup:
			{
				SetupDate.AddMonth(1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::YearSetup:
			{
				SetupDate.AddYear(1);
				ShowDate(SetupDate);
				break;
			}
		}
	}
	else if(SettingsState == ESettingsState::ChooseSettings && FocusedSetting == ESettingsState::AlarmSettings)
	{
		SetAlarmSettingsState(EAlarmSettingsState::MelodySettings);
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::MelodySettings)
		{
			SwitchFocusedSong(ESwitchDirection::Up);
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			switch(NewAlarmState)
			{
				case ENewAlarmState::HourSetup:
				{
					NewAlarm.Time.AddHour(1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::MinutesSetup:
				{
					NewAlarm.Time.AddMinute(1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::AlarmTypeSetup:
				{
					DClockFunctionLibrary::AddAlarmType(NewAlarm.Type, 1);
					ShowAlarmType(NewAlarm.Type);
					break;
				}
			}
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
		{
			SwitchFocusedAlarm(ESwitchDirection::Up);
		}
	}
}

void DClock::DownButtonPressed_Callback()
{
	if(SettingsState == ESettingsState::TimeSettings)
	{
		switch(TimeSettingsState)
		{
			case ETimeSettingsState::HourSetup:
			{
				SetupTime.AddHour(-1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::MinutesSetup:
			{
				SetupTime.AddMinute(-1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::DaySetup:
			{
				SetupDate.AddDay(-1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::MonthSetup:
			{
				SetupDate.AddMonth(-1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::YearSetup:
			{
				SetupDate.AddYear(-1);
				ShowDate(SetupDate);
				break;
			}
		}
	}
	else if(SettingsState == ESettingsState::ChooseSettings && FocusedSetting == ESettingsState::AlarmSettings)
	{
		SetAlarmSettingsState(EAlarmSettingsState::AlarmTimeSettings);
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::MelodySettings)
		{
			SwitchFocusedSong(ESwitchDirection::Down);
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			switch(NewAlarmState)
			{
				case ENewAlarmState::HourSetup:
				{
					NewAlarm.Time.AddHour(-1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::MinutesSetup:
				{
					NewAlarm.Time.AddMinute(-1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::AlarmTypeSetup:
				{
					DClockFunctionLibrary::AddAlarmType(NewAlarm.Type, -1);
					ShowAlarmType(NewAlarm.Type);
					break;
				}
			}
		}
		else if(AlarmSettingsState == EAlarmSettingsState::AlarmTimeSettings)
		{
			SwitchFocusedAlarm(ESwitchDirection::Down);
		}
	}
}

void DClock::UpButtonHoldDown_Callback()
{
	if(SettingsState == ESettingsState::TimeSettings)
	{
		switch(TimeSettingsState)
		{
			case ETimeSettingsState::HourSetup:
			{
				SetupTime.AddHour(1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::MinutesSetup:
			{
				SetupTime.AddMinute(10);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::DaySetup:
			{
				SetupDate.AddDay(10);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::MonthSetup:
			{
				SetupDate.AddYear(1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::YearSetup:
			{
				SetupDate.AddYear(10);
				ShowDate(SetupDate);
				break;
			}
		}
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			switch(NewAlarmState)
			{
				case ENewAlarmState::HourSetup:
				{
					NewAlarm.Time.AddHour(1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::MinutesSetup:
				{
					NewAlarm.Time.AddMinute(10);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::AlarmTypeSetup:
				{
					DClockFunctionLibrary::AddAlarmType(NewAlarm.Type, 1);
					ShowAlarmType(NewAlarm.Type);
					break;
				}
			}
		}
	}
}

void DClock::DownButtonHoldDown_Callback()
{
	if(SettingsState == ESettingsState::TimeSettings)
	{
		switch(TimeSettingsState)
		{
			case ETimeSettingsState::HourSetup:
			{
				SetupTime.AddHour(-1);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::MinutesSetup:
			{
				SetupTime.AddMinute(-10);
				ShowTime(SetupTime);
				break;
			}
			case ETimeSettingsState::DaySetup:
			{
				SetupDate.AddDay(-10);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::MonthSetup:
			{
				SetupDate.AddYear(-1);
				ShowDate(SetupDate);
				break;
			}
			case ETimeSettingsState::YearSetup:
			{
				SetupDate.AddYear(-10);
				ShowDate(SetupDate);
				break;
			}
		}
	}
	else if(SettingsState == ESettingsState::AlarmSettings)
	{
		if(AlarmSettingsState == EAlarmSettingsState::AddNewTime)
		{
			switch(NewAlarmState)
			{
				case ENewAlarmState::HourSetup:
				{
					NewAlarm.Time.AddHour(-1);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::MinutesSetup:
				{
					NewAlarm.Time.AddMinute(-10);
					ShowTime(NewAlarm.Time);
					break;
				}
				case ENewAlarmState::AlarmTypeSetup:
				{
					DClockFunctionLibrary::AddAlarmType(NewAlarm.Type, -1);
					ShowAlarmType(NewAlarm.Type);
					break;
				}
			}
		}
	}
}

DTask<StartRightNow> DClock::TimeSetupBlinkTask()
{
	while(SettingsState == ESettingsState::TimeSettings)
	{
		switch(TimeSettingsState)
		{
			case ETimeSettingsState::HourSetup:
			{
				Screen.DrawBitmap({0, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({30, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({72, 42}, {25, 43}, Numbers25_43 + SetupTime.GetHighMinutesNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({102, 42}, {25, 43}, Numbers25_43 + SetupTime.GetLowMinutesNum() * Number25_43_BuffSize);
				break;
			}
			case ETimeSettingsState::MinutesSetup:
			{
				Screen.DrawBitmap({0, 42}, {25, 43}, Numbers25_43 + SetupTime.GetHighHourNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({30, 42}, {25, 43}, Numbers25_43 + SetupTime.GetLowHourNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({72, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({102, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				break;
			}
			case ETimeSettingsState::DaySetup:
			{
				const auto MonthYearLength = Screen.GetTextLength((SetupDate.GetMonthName() + "  2000").c_str());
				Screen.DrawFillRect({127 - MonthYearLength - 14, 93}, {14, 11}, BLACK);
				break;
			}
			case ETimeSettingsState::MonthSetup:
			{
				const auto MonthNameLength = Screen.GetTextLength((SetupDate.GetMonthName() + ' ').c_str());
				Screen.DrawFillRect({127 - MonthNameLength - 28, 93}, {MonthNameLength, 11}, BLACK);
				break;
			}
			case ETimeSettingsState::YearSetup:
			{
				Screen.DrawFillRect({127 - 28, 93}, {28, 11}, BLACK);
				break;
			}
		};

		co_await DelayMillis(400);
		ShowTime(SetupTime);
		ShowDate(SetupDate);
		co_await DelayMillis(400);
	}
}

DTask<StartRightNow> DClock::NewAlarmBlinkTask()
{
	while(SettingsState == ESettingsState::AlarmSettings)
	{
		ShowTime(NewAlarm.Time);
		Screen.PrintString(DClockFunctionLibrary::AlarmTypeToString(NewAlarm.Type).c_str(), 93, ETextAlign::Center);
		co_await DelayMillis(400);
		if(SettingsState != ESettingsState::AlarmSettings) { break; }

		switch(NewAlarmState)
		{
			case ENewAlarmState::HourSetup:
			{
				Screen.DrawBitmap({0, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({30, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({72, 42}, {25, 43}, Numbers25_43 + NewAlarm.Time.GetHighMinutesNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({102, 42}, {25, 43}, Numbers25_43 + NewAlarm.Time.GetLowMinutesNum() * Number25_43_BuffSize);
				break;
			}
			case ENewAlarmState::MinutesSetup:
			{
				Screen.DrawBitmap({0, 42}, {25, 43}, Numbers25_43 + NewAlarm.Time.GetHighHourNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({30, 42}, {25, 43}, Numbers25_43 + NewAlarm.Time.GetLowHourNum() * Number25_43_BuffSize);
				Screen.DrawBitmap({72, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				Screen.DrawBitmap({102, 42}, {25, 43}, Numbers25_43 + 10 * Number25_43_BuffSize);
				break;
			}
			case ENewAlarmState::AlarmTypeSetup:
			{
				Screen.DrawFillRect({0, 93}, {128, 11}, BLACK);
				break;
			}
		};

		co_await DelayMillis(400);
	}
}

void DClock::SwitchFocusedSetting(ESwitchDirection InDirection)
{
	const int Offset = InDirection == ESwitchDirection::Left ? -1 : 1;
	const auto Position = static_cast<uint8_t>(FocusedSetting);
	auto NewPosition = Position + Offset;
	if(NewPosition < 1) { NewPosition = 1; }
	else if(NewPosition > 4) { NewPosition = 4; }

	FocusedSetting = static_cast<ESettingsState>(NewPosition);

	Screen.DrawRect({(Position - 1) * 18 + 2, 22}, {18, 18}, 1, BLACK);
	Screen.DrawRect({(NewPosition - 1) * 18 + 2, 22}, {18, 18}, 1, RED);
	Screen.DrawFillRect({0, 114}, {128, 11}, BLACK);
	Screen.PrintString(GetSettingsTextName(FocusedSetting).c_str(), 114, ETextAlign::Center);
	ClearSettingsScreen();

	switch(FocusedSetting)
	{
		case ESettingsState::TimeSettings:
		{
			Synchronizer.Synchronize();
			SetupTime = Time;
			SetupDate = Date;
			ShowTime(SetupTime);
			ShowDate(SetupDate);
			DrawColon(true);
			break;
		}
		case ESettingsState::AlarmSettings:
		{
			Screen.PrintString("�������", {4, 50});
			Screen.PrintString("�����", {4, 68});
			SetAlarmSettingsState(EAlarmSettingsState::MelodySettings);
			break;
		}
		case ESettingsState::WifiSettings:break;
		case ESettingsState::LightSettings:break;
		default:break;
	}
}

void DClock::SwitchFocusedSong(ESwitchDirection InDirection)
{
	Screen.DrawRect({2, FocusedSongIndex * 18 + 48}, {59, 15}, 1, BLACK);

	if(InDirection == ESwitchDirection::Down && FocusedSongIndex < Songs.size() - 1)
	{
		++FocusedSongIndex;
	}
	else if(InDirection == ESwitchDirection::Up && FocusedSongIndex > 0)
	{
		--FocusedSongIndex;
	}

	Screen.DrawRect({2, FocusedSongIndex * 18 + 48}, {59, 15}, 1, RED);

	Buzzer.Stop();
	Buzzer.PlaySong(Songs[FocusedSongIndex]);
}

void DClock::SwitchFocusedAlarm(ESwitchDirection InDirection)
{
	Screen.DrawRect({2, AlarmCursorPosition * 18 + 48}, {124, 15}, 1, BLACK);

	if(InDirection == ESwitchDirection::Down)
	{
		const auto AlarmAmount = Alarm.GetAlarmAmount();
		if(AlarmCursorPosition < (AlarmAmount > 3 ? 2 : AlarmAmount - 1))
		{
			++AlarmCursorPosition;
		}
		else if(AlarmsOffset + AlarmCursorPosition + 1 < AlarmAmount)
		{
			++AlarmsOffset;
			ShowAlarmTimeSettings();
			return;
		}
	}
	else if(InDirection == ESwitchDirection::Up)
	{
		if(AlarmCursorPosition > 0)
		{
			--AlarmCursorPosition;
		}
		else if(AlarmsOffset > 0)
		{
			--AlarmsOffset;
			ShowAlarmTimeSettings();
			return;
		}
	}

	Screen.DrawRect({2, AlarmCursorPosition * 18 + 48}, {124, 15}, 1, RED);
}

void DClock::SetAlarmSettingsState(EAlarmSettingsState InState)
{
	auto Position = static_cast<uint8_t>(AlarmSettingsState);
	Screen.DrawRect({2, Position * 18 + 48}, {59, 15}, 1, BLACK);

	AlarmSettingsState = InState;

	Position = static_cast<uint8_t>(AlarmSettingsState);
	Screen.DrawRect({2, Position * 18 + 48}, {59, 15}, 1, RED);
}

std::string DClock::GetSettingsTextName(ESettingsState InState)
{
	switch(InState)
	{
		case ESettingsState::TimeSettings: return "���� � �����";
		case ESettingsState::AlarmSettings: return "���������";
		case ESettingsState::WifiSettings: return "����"; //todo wifi
		case ESettingsState::LightSettings: return "���������";
		default: break;
	}
	return {};
}

void DClock::ShowMelodySettings()
{
	ClearSettingsScreen();
	for(uint8_t i = 0; i < Songs.size(); ++i)
	{
		DVector2D TextPoint{4, i * 18 + 50};
		Screen.PrintString(Songs[i].Name.c_str(), TextPoint);

		if(Song == Songs[i])
		{
			Screen.DrawBitmap({100, i * 18 + 50}, {15, 11}, CheckIcon);
		}
	}

	SwitchFocusedSong(ESwitchDirection::Up);
}

void DClock::ShowAlarmTimeSettings()
{
	ClearSettingsScreen();

	const auto AlarmAmount = Alarm.GetAlarmAmount();
	if(AlarmAmount == 0)
	{
		Screen.PrintString("��� �������������", 50, ETextAlign::Center);
		Screen.PrintString("�����������", 68, ETextAlign::Center);
	}
	else
	{
		auto AlarmsTimes = Alarm.GetAlarmInfo();
		auto MaxInScreen = AlarmAmount > 3 ? 3 + AlarmsOffset : AlarmAmount;
		for(uint8_t i = AlarmsOffset, j = 0; i < MaxInScreen; ++i, ++j)
		{
			const auto AlarmInfo = AlarmsTimes[i];
			std::string AlarmText = DClockFunctionLibrary::AlarmToString(AlarmInfo);
			Screen.PrintString(AlarmText.c_str(), {4, 50 + 18 * j});

			if(AlarmInfo.bIsActive)
			{
				Screen.DrawBitmap({108, j * 18 + 50}, {15, 11}, CheckIcon);
			}
			else
			{
				Screen.DrawFillRect({108, j * 18 + 50}, {15, 11}, BLACK);
			}
		}

		Screen.DrawRect({2, AlarmCursorPosition * 18 + 48}, {124, 15}, 1, RED);
	}
}

void DClock::ShowAddNewAlarmTimeScreen()
{
	AlarmSettingsState = EAlarmSettingsState::AddNewTime;
	NewAlarmState = ENewAlarmState::HourSetup;
	NewAlarm = DAlarmInfo{};
	ClearSettingsScreen();
	Screen.PrintString("����� ���������", 114, ETextAlign::Center);
	DrawColon(true);
	NewAlarmBlinkTask();
}

void DClock::ClearSettingsScreen() const
{
	Screen.DrawFillRect({0, 40}, {128, 69}, BLACK);
}

void DClock::ShowAlarmType(EAlarmType InType)
{
	Screen.DrawFillRect({0, 93}, {128, 11}, BLACK);
	Screen.PrintString(DClockFunctionLibrary::AlarmTypeToString(InType).c_str(), 93, ETextAlign::Center);
}

void DClock::TempUpdateTick()
{
	while(true)
	{
		TempSensor.UpdateTemperature();
		sleep_ms(1000);

		if(multicore_fifo_wready())
		{
			multicore_fifo_push_blocking(TempSensor.GetTemperature());
		}
	}
}

void DClock::ShowTemp()
{
	if(multicore_fifo_rvalid())
	{
		const auto Temp = multicore_fifo_pop_blocking();
		multicore_fifo_drain();
		if(Temp > 99) { return; }

		Screen.PrintString(std::to_string(Temp).c_str(), {14, 2});
	}
}

void DClock::DrawTempIcon()
{
	Screen.DrawBitmap({0, 0}, {33, 15}, TempIcon);
	ShowTemp();
}






























