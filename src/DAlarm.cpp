#ifdef DEBUG
	#include <cstdio>
#endif

#include "DAlarm.h"
#include <hardware/flash.h>

DAlarm::DAlarm()
{
	ReadInfoFromFlash();
}

void DAlarm::Check(const DDate& InDate, const DTime& InTime)
{
#ifdef DEBUG
	printf("Check Alarm\n");

	//������� � ������� ������ ���� ��������
	for(int i = 0; i < AlarmAmount; ++i)
	{
		printf("Time_%i - %i : %i %s\n"
			   , i + 1
			   , (AlarmTimes + i)->Time.Hours
			   , (AlarmTimes + i)->Time.Minutes
			   , (AlarmTimes + i)->Type == EAlarmType::EveryDay
				 ? "EveryDay"
				 : (AlarmTimes + i)->Type == EAlarmType::Weekdays
				   ? "Weekdays"
				   : "Once");
	}

	if(!AlarmAmount)
	{
		printf("No alarms\n");
	}
#endif

	const auto FoundAlarm = std::find_if(AlarmTimes, AlarmTimes + AlarmAmount, [&InDate, &InTime](const DAlarmInfo& AlarmInfo)
	{
		if(AlarmInfo.bIsActive && AlarmInfo.Time == InTime) //���� ����� ���������
		{
			if(AlarmInfo.Type == EAlarmType::Weekdays) //���� ��������� �������� �� ������ ���
			{
				return DClockFunctionLibrary::IsWeekday(InDate);
			}

			return true;
		}

		return false; //���� ����� �� ��������� ���������� false
	});

	if(FoundAlarm != AlarmTimes + AlarmAmount)
	{
		if(FoundAlarm->Type == EAlarmType::Once) //���� ��������� ������������ �� ������� ����� ������������
		{
			uint8_t Index = 0;
			for(; Index < AlarmAmount; ++Index)
			{
				const auto AlarmInfo = AlarmTimes[Index];
				if(FoundAlarm->Time == AlarmInfo.Time && FoundAlarm->Type == AlarmInfo.Type)
				{
					ToggleAlarm(Index);
					break;
				}
			}
		}
		OnAlarmTriggered.Broadcast();
	}
}

bool DAlarm::Add(const DAlarmInfo& InInfo)
{
	const auto FoundAlarm = std::find_if(AlarmTimes, AlarmTimes + AlarmAmount, [&InInfo](const DAlarmInfo& AlarmInfo)
	{
		return AlarmInfo.Time == InInfo.Time && AlarmInfo.Type == InInfo.Type;
	});

	if(FoundAlarm != AlarmTimes + AlarmAmount) { return false; } // ���������� false ���� ����� ��������� ��� ����������

	constexpr auto AlarmInfoSize = sizeof(DAlarmInfo);

	//������� ����� ��������� � ����� ��� ������ � Flash ������
	DAlarmInfo NewAlarm = InInfo;
	uint8_t NewAlarmsBuffer[FLASH_PAGE_SIZE];
	uint8_t* NewAlarmsBufferPtr = NewAlarmsBuffer;

	//���������� � ������ ���� ����� ���������� �����������
	*NewAlarmsBufferPtr = AlarmAmount + 1;
	++NewAlarmsBufferPtr;

	//��������� ������ ����������
	const auto CurrentAlarmsAsByte = reinterpret_cast<uint8_t*>(AlarmTimes);
	const auto AlarmsBytesCount = AlarmInfoSize * AlarmAmount;
	for(uint8_t i = 0; i < AlarmsBytesCount; ++i, ++NewAlarmsBufferPtr)
	{
		*NewAlarmsBufferPtr = CurrentAlarmsAsByte[i];
	}

	//��������� ����� ���������
	const auto NewAlarmAsByte = reinterpret_cast<uint8_t*>(&NewAlarm);
	for(uint8_t i = 0; i < AlarmInfoSize; ++i, ++NewAlarmsBufferPtr)
	{
		*NewAlarmsBufferPtr = NewAlarmAsByte[i];
	}

	flash_range_erase(ALARM_INFO_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(ALARM_INFO_OFFSET, NewAlarmsBuffer, FLASH_PAGE_SIZE);

	ReadInfoFromFlash();

	return true;
}

bool DAlarm::IsActive() const
{
	const auto FoundAlarm = std::find_if(AlarmTimes, AlarmTimes + AlarmAmount, [](const DAlarmInfo& AlarmInfo)
	{
		return AlarmInfo.bIsActive;
	});

	return FoundAlarm != AlarmTimes + AlarmAmount;
}

void DAlarm::ReadInfoFromFlash()
{
	uint8_t* AlarmFlashInfo = reinterpret_cast<uint8_t*>(XIP_BASE + ALARM_INFO_OFFSET);
	AlarmAmount = *AlarmFlashInfo; //������ ���� � AlarmFlashInfo ������������� ���������� ���������� �����������
	AlarmTimes = reinterpret_cast<DAlarmInfo*>(AlarmFlashInfo + 1); //������� �� ������� ����� ���������� ���������� � �����������
}

void DAlarm::Remove(uint8_t InIndex)
{
	if(InIndex >= AlarmAmount) { return; }

	constexpr auto AlarmInfoSize = sizeof(DAlarmInfo);

	//������� ����� ��� ������ � Flash ������
	uint8_t NewAlarmsBuffer[FLASH_PAGE_SIZE];
	uint8_t* NewAlarmsBufferPtr = NewAlarmsBuffer;

	//���������� � ������ ���� ����� ���������� �����������
	*NewAlarmsBufferPtr = AlarmAmount - 1;
	++NewAlarmsBufferPtr;

	//��������� ������ ���������� ����� ����������
	const auto CurrentAlarmsAsByte = reinterpret_cast<uint8_t*>(AlarmTimes);
	const auto AlarmsBytesCount = AlarmInfoSize * AlarmAmount;
	uint8_t CurrentIndex = 0;
	for(uint8_t i = 0; i < AlarmsBytesCount; ++i)
	{
		if(CurrentIndex != InIndex)
		{
			*NewAlarmsBufferPtr = CurrentAlarmsAsByte[i];
			++NewAlarmsBufferPtr;
		}
		if((i + 1) % AlarmInfoSize == 0) { ++CurrentIndex; }
	}

	flash_range_erase(ALARM_INFO_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(ALARM_INFO_OFFSET, NewAlarmsBuffer, FLASH_PAGE_SIZE);

	ReadInfoFromFlash();
}

void DAlarm::RemoveAll()
{
	uint8_t NewAlarmsBuffer[FLASH_PAGE_SIZE] = {0};
	flash_range_erase(ALARM_INFO_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(ALARM_INFO_OFFSET, NewAlarmsBuffer, FLASH_PAGE_SIZE);
	ReadInfoFromFlash();
}

void DAlarm::ToggleAlarm(uint8_t InIndex)
{
	DAlarmInfo NewAlarm = AlarmTimes[InIndex];
	NewAlarm.bIsActive = !NewAlarm.bIsActive;

	constexpr auto AlarmInfoSize = sizeof(DAlarmInfo);
	uint8_t NewAlarmsBuffer[FLASH_PAGE_SIZE];
	uint8_t* NewAlarmsBufferPtr = NewAlarmsBuffer;

	//���������� � ������ ���� ����� ���������� �����������
	*NewAlarmsBufferPtr = AlarmAmount;
	++NewAlarmsBufferPtr;

	//�������������� ����������
	const auto CurrentAlarmsAsByte = reinterpret_cast<uint8_t*>(AlarmTimes);
	const auto NewAlarmAsByte = reinterpret_cast<uint8_t*>(&NewAlarm);
	const auto AlarmsBytesCount = AlarmInfoSize * AlarmAmount;
	uint8_t CurrentIndex = 0;
	for(uint8_t i = 0, j = 0; i < AlarmsBytesCount; ++i)
	{
		if(CurrentIndex == InIndex)
		{
			*NewAlarmsBufferPtr = NewAlarmAsByte[j];
			++j;
		}
		else
		{
			*NewAlarmsBufferPtr = CurrentAlarmsAsByte[i];
		}

		++NewAlarmsBufferPtr;

		if((i + 1) % AlarmInfoSize == 0) { ++CurrentIndex; }
	}

	flash_range_erase(ALARM_INFO_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(ALARM_INFO_OFFSET, NewAlarmsBuffer, FLASH_PAGE_SIZE);

	ReadInfoFromFlash();
}


