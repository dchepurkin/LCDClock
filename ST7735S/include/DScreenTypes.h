#pragma once

#include <pico/stdlib.h>
#include "DVector2D.h"

enum class ETextAlign : uint8_t
{
	Left,
	Right,
	Center,
};

enum class ERotation
{
	Degree0 = 0xD8,    //0 degrees
	Degree90 = 0xA8,    //90 degrees
	Degree180 = 0x08,    //180 degrees
	Degree270 = 0x68    //270 degrees
};

struct DScreenInfo
{
	uint8_t PinSDA = 7;

	uint8_t PinSCK = 6;

	uint8_t PinCS = 5;

	uint8_t PinA0 = 4;

	uint8_t PinRST = 11;

	uint8_t PinLED = 3;

	ERotation Rotation = ERotation::Degree0;

	float SerialClkDiv = 1.0;

	DVector2D Size{128, 128};
};