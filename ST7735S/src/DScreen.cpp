#include "DScreen.h"
#include "ST7735S_Driver.pio.h"
#include "DColors.h"
#include "FontBlack.h"

#ifdef DEBUG
	#include <cstdio>
#endif

DScreen::DScreen(const DScreenInfo& InScreenInfo)
{
	ScreenInfo = InScreenInfo;

	uint Offset = pio_add_program(Pio, &ST7735S_Driver_program);
	ST7735S_Driver_program_init(Pio, SMNum, Offset, ScreenInfo.PinSDA, ScreenInfo.PinSCK, ScreenInfo.SerialClkDiv);

	gpio_init(ScreenInfo.PinCS);
	gpio_init(ScreenInfo.PinA0);
	gpio_init(ScreenInfo.PinRST);
	gpio_init(ScreenInfo.PinLED);
	gpio_set_dir(ScreenInfo.PinCS, GPIO_OUT);
	gpio_set_dir(ScreenInfo.PinA0, GPIO_OUT);
	gpio_set_dir(ScreenInfo.PinRST, GPIO_OUT);
	gpio_set_dir(ScreenInfo.PinLED, GPIO_OUT);

	gpio_put(ScreenInfo.PinCS, true);
	gpio_put(ScreenInfo.PinRST, true);
	gpio_put(ScreenInfo.PinLED, false);

	InitScreen();
	FillScreen(WHITE);

	SetLedEnabled(true);
}

void DScreen::SetLedEnabled(const bool IsEnabled) const
{
	gpio_put(ScreenInfo.PinLED, IsEnabled);
}

void DScreen::InitScreen() const
{
	const uint8_t InitSeq[] = {
			1, 10, 0x11, //
			4, 0, 0xB1, 0x05, 0x3C, 0x3C, //
			4, 0, 0xB2, 0x05, 0x3C, 0x3C, //
			7, 0, 0xB3, 0x05, 0x3C, 0x3C, 0x05, 0x3C, 0x3C, //
			2, 0, 0xB4, 0x03, //
			4, 0, 0xC0, 0x28, 0x08, 0x04, //
			2, 0, 0xC1, 0xC0, //
			3, 0, 0xC2, 0x0D, 0x00, //
			3, 0, 0xC3, 0x8D, 0x2A, //
			3, 0, 0xC4, 0x8D, 0xEE, //
			2, 0, 0xC5, 0x1A, //
			2, 0, 0x17, 0x05, //
			2, 0, 0x36, 0xD8, //
			17, 0, 0xE0, 0x03, 0x22, 0x07, 0x0A, 0x2E, 0x30, 0x25, 0x2A, 0x28, 0x26, 0x2E, 0x3A, 0x00, 0x01, 0x03, 0x13, //
			17, 0, 0xE1, 0x04, 0x16, 0x06, 0x0D, 0x2D, 0x26, 0x23, 0x27, 0x27, 0x25, 0x2D, 0x3B, 0x00, 0x01, 0x04, 0x13, //
			2, 0, 0x3A, 0x05, //
			1, 0, 0x29, //
			0 //
	};

	WriteSeq(InitSeq);
}

void DScreen::WriteSeq(const uint8_t* InSeq) const
{
	while(*InSeq)
	{
		WriteCMD(InSeq + 2, *InSeq);
		//sleep_ms(*(InSeq + 1) * 5);
		InSeq += *InSeq + 2;
	}
}

void DScreen::WriteCMD(const uint8_t* InCmd, size_t InCount) const
{
	ST7735S_Driver_wait_idle(Pio, SMNum);
	SetDCCS(false, false);
	ST7735S_Driver_put(Pio, SMNum, *InCmd++);

	if(InCount >= 2)
	{
		ST7735S_Driver_wait_idle(Pio, SMNum);
		SetDCCS(true, false);
		for(uint i = 0; i < InCount - 1; ++i)
		{
			ST7735S_Driver_put(Pio, SMNum, *InCmd++);
		}
	}

	ST7735S_Driver_wait_idle(Pio, SMNum);
	SetDCCS(true, true);
}

void DScreen::SetDCCS(bool DC, bool CS) const
{
	//sleep_us(1);
	gpio_put_masked((1u << ScreenInfo.PinA0) | (1u << ScreenInfo.PinCS), DC << ScreenInfo.PinA0 | CS << ScreenInfo.PinCS);
	//sleep_us(1);
}

void DScreen::StartPixels(const DVector2D& InPoint1, const DVector2D& InPoint2) const
{
	int XOffset;
	int YOffset;

	switch(ScreenInfo.Rotation)
	{
		case ERotation::Degree0:
		{
			XOffset = 2;
			YOffset = 3;
			break;
		}

		case ERotation::Degree90:
		{
			XOffset = 3;
			YOffset = 2;
			break;
		}

		case ERotation::Degree180:
		{
			XOffset = 2;
			YOffset = 1;
			break;
		}

		case ERotation::Degree270:
		{
			XOffset = 1;
			YOffset = 2;
			break;
		}
	}

	const uint8_t Seq[] = {
			5, 0, 0x2a, uint8_t((InPoint1.X + XOffset) >> 8), uint8_t((InPoint1.X + XOffset) & 0xFF), (uint8_t)((InPoint2.X + XOffset) >> 8), (uint8_t)(
					(InPoint2.X + XOffset) & 0xFF), //
			5, 0, 0x2b, uint8_t((InPoint1.Y + YOffset) >> 8), uint8_t((InPoint1.Y + YOffset) & 0xFF), (uint8_t)((InPoint2.Y + YOffset) >> 8), (uint8_t)(
					(InPoint2.Y + YOffset) & 0xFF), //
			1, 0, 0x2C, //
			0           //
	};

	WriteSeq(Seq);
	SetDCCS(true, false);
}

void DScreen::DrawBitmap(const DVector2D& InPoint, const DVector2D& InSize, const uint16_t* InBitmap) const
{
	StartPixels(InPoint, InPoint + InSize - DVector2D{1, 1});

	const auto PixelsCount = InSize.X * InSize.Y;
	for(int i = 0; i < PixelsCount; ++i)
	{
		PutPixel(InBitmap[i]);
	}

	SetDCCS(true, true);
}

void DScreen::DrawPixel(const DVector2D& InPoint, const uint16_t InColor) const
{
	StartPixels(InPoint, InPoint);
	PutPixel(InColor);
	SetDCCS(true, true);
}

void DScreen::DrawHLine(const DVector2D& InStartPoint, const int InLength, const uint16_t InColor) const
{
	if(InStartPoint.Y >= GetHeight())
	{
		return;
	}

	auto FinishX = InStartPoint.X + InLength;
	const auto Width = GetWidth();

	if(FinishX > Width)
	{
		FinishX = Width;
	}

	StartPixels(InStartPoint, {FinishX, InStartPoint.Y});

	for(int X = InStartPoint.X; X < FinishX; ++X)
	{
		PutPixel(InColor);
	}

	SetDCCS(true, true);
}

void DScreen::DrawVLine(const DVector2D& InStartPoint, const int InLength, const uint16_t InColor) const
{
	if(InStartPoint.X >= GetWidth())
	{
		return;
	}

	auto FinishY = InStartPoint.Y + InLength;
	const auto Height = GetHeight();
	if(FinishY > Height)
	{
		FinishY = Height;
	}

	StartPixels(InStartPoint, {InStartPoint.X, FinishY});

	for(int Y = InStartPoint.Y; Y < FinishY; ++Y)
	{
		PutPixel(InColor);
	}

	SetDCCS(true, true);
}

void DScreen::FillScreen(const uint16_t InColor) const
{
	DrawFillRect(DVector2D{}, GetScreenSize(), InColor);
}

void DScreen::DrawFillRect(const DVector2D& InPoint, const DVector2D& InSize, const uint16_t InColor) const
{
	const auto Height = GetHeight();
	const auto Width = GetWidth();

	if(InPoint.X >= Width || InPoint.Y >= Height)
	{
		return;
	}

	auto FinishPoint = InPoint + InSize - DVector2D{1, 1};

	StartPixels(InPoint, FinishPoint);

	const auto PixelsCount = InSize.X * InSize.Y;

	for(uint i = 0; i < PixelsCount; ++i)
	{
		PutPixel(InColor);
	}

	SetDCCS(true, true);
}

void DScreen::PutPixel(const uint16_t InColor) const
{
	ST7735S_Driver_put(Pio, SMNum, InColor >> 8);
	ST7735S_Driver_put(Pio, SMNum, InColor & 0xFF);
}

void DScreen::SetRotation(ERotation InRotation)
{
	ScreenInfo.Rotation = InRotation;

	const uint8_t Seq[] = {
			2, 0, 0x36, static_cast<uint8_t>(ScreenInfo.Rotation), //
			0
	};

	WriteSeq(Seq);
	SetDCCS(true, false);
}

void DScreen::DrawRect(const DVector2D& InPoint, const DVector2D& InSize, const int InBorderSize, const uint16_t InBorderColor, const bool Fill, const uint16_t InFillColor) const
{
	if(InBorderColor == InFillColor || InBorderSize * 2 >= InSize.X || InBorderSize * 2 >= InSize.Y)
	{
		DrawFillRect(InPoint, InSize, InBorderColor);
		return;
	}

	if(InBorderSize == 0)
	{
		if(Fill)
		{
			DrawFillRect(InPoint, InSize, InFillColor);
		}

		return;
	}

	// Top Border
	DVector2D Point(InPoint);
	DVector2D Size(InSize.X, InBorderSize);
	DrawFillRect(Point, Size, InBorderColor);

	// Left Border
	Point.Set(InPoint.X, InPoint.Y + InBorderSize);
	Size.Set(InBorderSize, InSize.Y - InBorderSize * 2);
	DrawFillRect(Point, Size, InBorderColor);

	// Right Border
	Point.Set(InPoint.X + InSize.X - InBorderSize, InPoint.Y + InBorderSize);
	Size.Set(InBorderSize, InSize.Y - InBorderSize * 2);
	DrawFillRect(Point, Size, InBorderColor);

	// Bottom Border
	Point.Set(InPoint.X, InPoint.Y + InSize.Y - InBorderSize);
	Size.Set(InSize.X, InBorderSize);
	DrawFillRect(Point, Size, InBorderColor);

	// Fill
	if(Fill)
	{
		Point.Set(InPoint.X + InBorderSize, InPoint.Y + InBorderSize);
		Size.Set(InSize.X - 2 * InBorderSize, InSize.Y - 2 * InBorderSize);
		DrawFillRect(Point, Size, InFillColor);
	}
}

void DScreen::PrintString(const char* InString, const DVector2D& InPoint)
{
	constexpr uint8_t CharSize = 11; //������ � ������ ������� � ������� FontBlack
	const auto TextLength = GetTextLength(InString);
	uint16_t StringBuffer[CharSize * TextLength];

	uint8_t CurrentLength = 0;

	for(; *InString != '\0'; ++InString)
	{
		auto CharPtr = GetCharStartPointer(*InString);
		const auto CharWidth = *(CharPtr++); //+1 �.�. ������ ������� �������� ���������� � ������

		for(uint8_t H = 0; H < CharSize; ++H)
		{
			for(uint8_t W = CurrentLength; W < CurrentLength + CharWidth; ++W, ++CharPtr)
			{
				const auto BufferIndex = H * TextLength + W;
				StringBuffer[BufferIndex] = *CharPtr;
			}

			CharPtr += CharSize - CharWidth; //���������� ��������� �� ��������� ������
		}

		CurrentLength += CharWidth;
	}

	DVector2D TextSize{TextLength, CharSize};
	DrawBitmap(InPoint, TextSize, StringBuffer);
}

void DScreen::PrintString(const char* InString, const int InY, ETextAlign InAlign)
{
	const auto TextLength = GetTextLength(InString);

	const auto X = InAlign == ETextAlign::Right
				   ? GetWidth() - TextLength
				   : InAlign == ETextAlign::Left
					 ? 0
					 : (GetWidth() - TextLength) / 2;

	DVector2D TextPosition{X, InY};
	PrintString(InString, TextPosition);
}

uint8_t DScreen::GetTextLength(const char* InString)
{
	uint8_t Result = 0;
	auto String = InString;

	for(; *String != '\0'; ++String)
	{
		if(const auto StartPtr = GetCharStartPointer(*String))
		{
			Result += *StartPtr;
		}
	}

	return Result;
}

const uint16_t* DScreen::GetCharStartPointer(const uint8_t InChar)
{
	if(InChar == 32) { return FontBlack; }

	constexpr uint8_t CharSize = 122;

	if(InChar >= 48 && InChar <= 57) { return FontBlack + ((InChar - 47) * CharSize); }

	if(InChar >= 192 && InChar <= 223) { return FontBlack + ((InChar - 181) * CharSize); }
	if(InChar >= 224 && InChar <= 255) { return FontBlack + ((InChar - 213) * CharSize); }

	return nullptr;
}








