#pragma once

#include <vector>
#include <string>

enum ETone : uint32_t
{
	Pause = 0,
	//1 ������
	A1 = 9008,
	Ais1 = 8580,
	B1 = 8098,

	//2 ������
	C2 = 7644,
	Cis2 = 7214,
	D2 = 6810,
	Dis2 = 6427,
	E2 = 6066,
	F2 = 5726,
	Fis2 = 5404,
	G2 = 5101,
	Gis2 = 4815,
	A2 = 4544,
	Ais2 = 4289,
	B2 = 4049,

	//3 ������
	C3 = 3821,
	Cis3 = 3607,
	D3 = 3404,
	Dis3 = 3213,
	E3 = 3033,
	F3 = 2862,
	Fis3 = 2702,
	G3 = 2550,
	Gis3 = 2407,
	A3 = 2272,
	Ais3 = 2144,
	B3 = 2024,

	//4 ������
	C4 = 1910,
	Cis4 = 1803,
	D4 = 1702,
	Dis4 = 1606,
	E4 = 1516,
	F4 = 1431,
	Fis4 = 1350,
	G4 = 1275,
	Gis4 = 1203,
	A4 = 1135,
	Ais4 = 1072,
	B4 = 1011,

	//5 ������
	C5 = 955,
	Cis5 = 901,
	D5 = 851,
	Dis5 = 803,
	E5 = 758,
	F5 = 715,
	Fis5 = 675,
	G5 = 638,
	Gis5 = 602,
	A5 = 568,
	Ais5 = 536,
	B5 = 506,

	//6 ������
	C6 = 478,
	Cis6 = 451,
	D6 = 426,
	Dis6 = 402,
	E6 = 379,
	F6 = 356,
	Fis6 = 378,
	G6 = 319,
	Gis6 = 301,
	A6 = 284,
	Ais6 = 268,
	B6 = 253,
};

enum ENoteDuration
{
	Dur1 = 32, // ����� ����
	Dur2 = 16, // 1/2
	Dur4 = 8, // 1/4
	Dur8 = 4, // 1/8
	Dur16 = 2, // 1/16
	Dur32 = 1, // 1/32
};

struct DNote
{
	DNote() = delete;

	explicit DNote(ETone InTone, ENoteDuration InDuration)
			: Tone(InTone),
			  Duration(InDuration) {}

	ETone Tone;

	ENoteDuration Duration;
};

struct DMelody
{
	DMelody() = delete;

	DMelody(const uint8_t InTemp, std::vector<DNote>&& InNotes)
			: Temp(InTemp),
			  Notes(std::move(InNotes)) {}

	std::vector<DNote> Notes;

	uint8_t Temp = 120;
};

struct DSong
{
	DSong() = delete;

	DSong(const DSong& Other) = default;

	explicit DSong(const DMelody& InHighMelody, const DMelody& InLowMelody, const std::string& InName)
			: HighMelody(InHighMelody),
			  LowMelody(InLowMelody),
			  Name(InName) {}

	bool operator==(const DSong& Other) const
	{
		return Name == Other.Name;
	}

	DMelody HighMelody;

	DMelody LowMelody;

	std::string Name;
};
