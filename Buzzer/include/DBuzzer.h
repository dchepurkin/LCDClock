#pragma once

#include <pico/stdlib.h>
#include "DNotes.h"
#include "DCoroutine.h"

class DBuzzer
{
public:
	DBuzzer() = delete;

	explicit DBuzzer(const uint InPin);

	void PlayMelody(const DMelody& InMelody, bool IsLooped = false);

	void PlayTone(ETone InNote);

	bool IsPlaying() const { return bIsTonePlayed || bIsMelodyPlayed; }

	void Stop();

private:
	const uint Pin;

	bool bIsTonePlayed = false;

	bool bIsMelodyPlayed = false;

	void StopTone();

	ETone CurrentTone = ETone::Pause;

	DTask<StartRightNow> PlayTone_Task();

	DTask<StartRightNow> PlayMelody_Task(const DMelody& InMelody, bool IsLooped);
};