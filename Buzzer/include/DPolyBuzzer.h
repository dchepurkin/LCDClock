#pragma once

#include <pico/stdlib.h>
#include "DBuzzer.h"
#include "DNotes.h"

class DPolyBuzzer
{
public:
	DPolyBuzzer() = delete;

	explicit DPolyBuzzer(const uint InHighBuzzerPin, const uint InLowBuzzerPin)
			: HighBuzzer(DBuzzer{InHighBuzzerPin}),
			  LowBuzzer(DBuzzer{InLowBuzzerPin}) {}

	void PlaySong(const DSong& InSong, bool IsLooped = false);

	bool IsPlaying() const { return HighBuzzer.IsPlaying() || LowBuzzer.IsPlaying(); }

	void Stop();

private:
	DBuzzer HighBuzzer;

	DBuzzer LowBuzzer;

	DTask<StartRightNow> PlaySong_Task(const DSong& InSong, bool IsLooped = false);
};