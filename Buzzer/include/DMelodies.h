#pragma once

#include <pico/stdlib.h>
#include "DNotes.h"

static DMelody ElochkaHigh{90, //
		{DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{C5, Dur4},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				//
				DNote{F4, Dur4},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
		}};//


static DMelody ElochkaLow{90, //
		{DNote{Pause, Dur8},//
				//
				DNote{F3, Dur4},//
				DNote{C3, Dur4},//
				//
				DNote{F3, Dur2},//
				//
				DNote{F3, Dur4},//
				DNote{C3, Dur4},//
				//
				DNote{F3, Dur8},//
				DNote{G3, Dur8},//
				DNote{F3, Dur4},//
				//
				DNote{Ais3, Dur2},//
				//
				DNote{A3, Dur4},//
				DNote{D3, Dur4},//
				//
				DNote{F3, Dur4},//
				DNote{C3, Dur4},//
				//
				DNote{F3, Dur8},//
				DNote{C3, Dur8},//
				DNote{F3, Dur8},//
				//
		}};//

static DMelody CrazyFrogHigh{120, //
		{//
				//2
				DNote{E4, Dur4},//
				DNote{G4, Dur8},//
				DNote{G4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur8},//
				DNote{D4, Dur8},//

				//3
				DNote{E4, Dur4},//
				DNote{B4, Dur8},//
				DNote{B4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur8},//
				DNote{G4, Dur8},//

				//4
				DNote{E4, Dur8},//
				DNote{B4, Dur8},//
				DNote{E5, Dur8},//
				DNote{E5, Dur16},//
				DNote{D4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{B3, Dur8},//
				DNote{G4, Dur8},//
				DNote{E4, Dur8},//

				//5
				DNote{E4, Dur2},//
				DNote{E4, Dur4},//
				DNote{E4, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//

				//2_2
				DNote{E4, Dur4},//
				DNote{G4, Dur8},//
				DNote{G4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur8},//
				DNote{D4, Dur8},//

				//3_2
				DNote{E4, Dur4},//
				DNote{B4, Dur8},//
				DNote{B4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur8},//
				DNote{G4, Dur8},//

				//4_2
				DNote{E4, Dur8},//
				DNote{B4, Dur8},//
				DNote{E5, Dur8},//
				DNote{E5, Dur16},//
				DNote{D4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{B3, Dur8},//
				DNote{G4, Dur8},//
				DNote{E4, Dur8},//

				//5_2
				DNote{E4, Dur2},//
				DNote{E4, Dur4},//
				DNote{E4, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//

				//6
				DNote{Pause, Dur1},

				//7
				DNote{Pause, Dur1},

				//8
				DNote{Pause, Dur1},

				//9
				DNote{Pause, Dur1},

				//6_1
				DNote{Pause, Dur1},

				//7_1
				DNote{Pause, Dur1},

				//8_1
				DNote{Pause, Dur1},

				//9_1
				DNote{Pause, Dur1},

				//10
				DNote{E4, Dur4}, //
				DNote{G4, Dur8}, //
				DNote{G4, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{A4, Dur8}, //
				DNote{E4, Dur8}, //
				DNote{D4, Dur8}, //

				//11
				DNote{E4, Dur4}, //
				DNote{B4, Dur8}, //
				DNote{B4, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{C5, Dur8}, //
				DNote{B4, Dur8}, //
				DNote{G4, Dur8}, //

				//12
				DNote{E4, Dur8}, //
				DNote{B4, Dur8}, //
				DNote{E5, Dur8}, //
				DNote{E5, Dur16}, //
				DNote{D4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D4, Dur16}, //
				DNote{B3, Dur8}, //
				DNote{G4, Dur8}, //
				DNote{E4, Dur8}, //

				//13
				DNote{E4, Dur4}, //
				DNote{Pause, Dur2}, //
				DNote{Pause, Dur4}, //

				//10_2
				DNote{E4, Dur4}, //
				DNote{G4, Dur8}, //
				DNote{G4, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{A4, Dur8}, //
				DNote{E4, Dur8}, //
				DNote{D4, Dur8}, //

				//11_2
				DNote{E4, Dur4}, //
				DNote{B4, Dur8}, //
				DNote{B4, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{E4, Dur16}, //
				DNote{C5, Dur8}, //
				DNote{B4, Dur8}, //
				DNote{G4, Dur8}, //

				//12_2
				DNote{E4, Dur8}, //
				DNote{B4, Dur8}, //
				DNote{E5, Dur8}, //
				DNote{E5, Dur16}, //
				DNote{D4, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D4, Dur16}, //
				DNote{B3, Dur8}, //
				DNote{G4, Dur8}, //
				DNote{E4, Dur8}, //

				//13_2
				DNote{E4, Dur4}, //
				DNote{Pause, Dur2}, //
				DNote{Pause, Dur4}, //

		}};//

static DMelody CrazyFrogLow{120, //
		{//
				//2
				DNote{Pause, Dur1},

				//3
				DNote{Pause, Dur1},

				//4
				DNote{Pause, Dur1},

				//5
				DNote{Pause, Dur1},

				//2_2
				DNote{Pause, Dur1},

				//3_2
				DNote{Pause, Dur1},

				//4_2
				DNote{Pause, Dur1},

				//5_2
				DNote{Pause, Dur1},

				//6
				DNote{E2, Dur4},//
				DNote{E3, Dur8},//
				DNote{E3, Dur16},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur16},//
				DNote{B2, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B2, Dur8},//
				DNote{D3, Dur8},//

				//7
				DNote{E2, Dur4},//
				DNote{E3, Dur8},//
				DNote{E3, Dur16},//
				DNote{B2, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B2, Dur16},//
				DNote{D3, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur8},//

				//8
				DNote{D3, Dur8},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur8},//
				DNote{D3, Dur16},//
				DNote{B2, Dur8},//
				DNote{D3, Dur16},//
				DNote{E3, Dur8},//
				DNote{E3, Dur4},//

				//9
				DNote{E2, Dur4},//
				DNote{E2, Dur4},//
				DNote{D3, Dur16},//
				DNote{B2, Dur8},//
				DNote{A2, Dur8},//
				DNote{G2, Dur8},//
				DNote{G2, Dur16},//

				//6_1
				DNote{E2, Dur4},//
				DNote{E3, Dur8},//
				DNote{E3, Dur16},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur16},//
				DNote{B2, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B2, Dur8},//
				DNote{D3, Dur8},//

				//7_1
				DNote{E2, Dur4},//
				DNote{E3, Dur8},//
				DNote{E3, Dur16},//
				DNote{B2, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B2, Dur16},//
				DNote{D3, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur8},//

				//8_1
				DNote{D3, Dur8},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur8},//
				DNote{D3, Dur16},//
				DNote{B2, Dur8},//
				DNote{D3, Dur16},//
				DNote{E3, Dur8},//
				DNote{E3, Dur4},//

				//9_1
				DNote{E2, Dur4},//
				DNote{E2, Dur4},//
				DNote{D3, Dur16},//
				DNote{B2, Dur8},//
				DNote{A2, Dur8},//
				DNote{G2, Dur8},//
				DNote{G2, Dur16},//

				//10
				DNote{E2, Dur4}, //
				DNote{D3, Dur8}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{D3, Dur8}, //

				//11
				DNote{E2, Dur4}, //
				DNote{E3, Dur8}, //
				DNote{E3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{Pause, Dur8}, //
				DNote{E3, Dur8}, //
				DNote{D3, Dur8}, //
				DNote{E3, Dur8}, //

				//12
				DNote{C3, Dur8}, //
				DNote{Pause, Dur8}, //
				DNote{C3, Dur8}, //
				DNote{C3, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{D3, Dur8}, //
				DNote{E3, Dur8}, //

				//13
				DNote{E3, Dur4}, //
				DNote{E2, Dur4}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{A2, Dur8}, //
				DNote{G2, Dur8}, //
				DNote{G2, Dur16}, //

				//10_2
				DNote{E2, Dur4}, //
				DNote{D3, Dur8}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{D3, Dur8}, //

				//11_2
				DNote{E2, Dur4}, //
				DNote{E3, Dur8}, //
				DNote{E3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{Pause, Dur8}, //
				DNote{E3, Dur8}, //
				DNote{D3, Dur8}, //
				DNote{E3, Dur8}, //

				//12_2
				DNote{C3, Dur8}, //
				DNote{Pause, Dur8}, //
				DNote{C3, Dur8}, //
				DNote{C3, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{Pause, Dur16}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{D3, Dur8}, //
				DNote{E3, Dur8}, //

				//13_2
				DNote{E3, Dur4}, //
				DNote{E2, Dur4}, //
				DNote{D3, Dur16}, //
				DNote{B2, Dur8}, //
				DNote{A2, Dur8}, //
				DNote{G2, Dur8}, //
				DNote{G2, Dur16}, //
		}};//

static DMelody SuperMarioHigh{105, //
		{//
				//1
				DNote{E5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//2
				DNote{C5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//

				//3
				DNote{G4, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur8},//

				//4
				DNote{C5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//

				//5
				DNote{G4, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur8},//

				//6
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis4, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//

				//7
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C6, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C6, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C6, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//8
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis4, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//

				//9
				DNote{Pause, Dur8},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//10
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis4, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//

				//11
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C6, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C6, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C6, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//12
				DNote{Pause, Dur8},//
				DNote{G5, Dur16},//
				DNote{Fis5, Dur16},//
				DNote{F5, Dur16},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis4, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//

				//13
				DNote{Pause, Dur8},//
				DNote{Dis5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//14
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//15
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur2},//

				//16
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//17
				DNote{E5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//18
				DNote{C5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//

				//19
				DNote{G4, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur8},//

				//20
				DNote{C5, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Ais4, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur16},//

				//21
				DNote{G4, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{B4, Dur16},//
				DNote{Pause, Dur8},//

				//22
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//23
				DNote{B4, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{A5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//24
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//25
				DNote{B4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//26
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//27
				DNote{B4, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{A5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//28
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//29
				DNote{B4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//30
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//31
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur2},//

				//32
				DNote{C5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{D5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//33
				DNote{E5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//34
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//35
				DNote{B4, Dur16},//
				DNote{A5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{A5, Dur16},//
				DNote{G5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//36
				DNote{E5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur16},//
				DNote{A4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//37
				DNote{B4, Dur16},//
				DNote{F5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F5, Dur32},//
				DNote{Pause, Dur32},//
				DNote{F5, Dur16},//
				DNote{E5, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D5, Dur16},//
				DNote{C5, Dur16},//
				DNote{Pause, Dur4},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//
		}
};

static DMelody SuperMarioLow{105, //
		{//
				//1
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//2
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Fis3, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//3
				DNote{E3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A3, Dur16},//
				DNote{B3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//

				//4
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Fis3, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//5
				DNote{E3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A3, Dur16},//
				DNote{B3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//

				//6
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//7
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//

				//8
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//9
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Ais3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//

				//10
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//11
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//

				//12
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//13
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Ais3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur16},//

				//14
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//15
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//16
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//17
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//18
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Fis3, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//19
				DNote{E3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A3, Dur16},//
				DNote{B3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//

				//20
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{E3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{Fis3, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//21
				DNote{E3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{E4, Dur16},//
				DNote{F4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D4, Dur16},//
				DNote{E4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{A3, Dur16},//
				DNote{B3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//

				//22
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//23
				DNote{D3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//

				//24
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//25
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{A3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//26
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//27
				DNote{D3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//

				//28
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//29
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{A3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//30
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//31
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//32
				DNote{Gis2, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Dis3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Gis3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{G2, Dur16},//
				DNote{Pause, Dur16},//

				//33
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{D3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{D3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G4, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//

				//34
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//35
				DNote{D3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{F3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//

				//36
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Fis3, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C4, Dur32},//
				DNote{Pause, Dur32},//
				DNote{C4, Dur16},//
				DNote{F3, Dur16},//
				DNote{Pause, Dur16},//

				//37
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur32},//
				DNote{Pause, Dur32},//
				DNote{G3, Dur16},//
				DNote{A3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{B3, Dur16},//
				DNote{C4, Dur16},//
				DNote{Pause, Dur16},//
				DNote{G3, Dur16},//
				DNote{Pause, Dur16},//
				DNote{C3, Dur16},//
				DNote{Pause, Dur8},//
				DNote{Pause, Dur16},//
		}
};

static const DSong Elochka{ElochkaHigh, ElochkaLow, "������"};
static const DSong CrazyFrog{CrazyFrogHigh, CrazyFrogLow, "�������"};
static const DSong SuperMario{SuperMarioHigh, SuperMarioLow, "�����"};
