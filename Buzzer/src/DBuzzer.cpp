#include "DBuzzer.h"
#include "DNotes.h"

#ifdef DEBUG
	#include <cstdio>
#endif

DBuzzer::DBuzzer(const uint InPin)
		: Pin(InPin)
{
	gpio_init(Pin);
	gpio_set_dir(Pin, GPIO_OUT);
}

void DBuzzer::PlayTone(ETone InNote)
{
	CurrentTone = InNote;
	if(CurrentTone == ETone::Pause)
	{
		StopTone();
		return;
	}

	if(!bIsTonePlayed)
	{
		PlayTone_Task();
	}
}

DTask<StartRightNow> DBuzzer::PlayTone_Task()
{
	bIsTonePlayed = true;

	while(bIsTonePlayed)
	{
		gpio_put(Pin, !gpio_get(Pin));
		co_await DelayMicros(CurrentTone);
	}
}

void DBuzzer::Stop()
{
	bIsMelodyPlayed = false;
	StopTone();
}

void DBuzzer::StopTone()
{
	bIsTonePlayed = false;
	CurrentTone = ETone::Pause;
	gpio_put(Pin, false);
}

void DBuzzer::PlayMelody(const DMelody& InMelody, bool IsLooped)
{
	if(bIsTonePlayed || bIsMelodyPlayed) { return; }
	PlayMelody_Task(InMelody, IsLooped);
}

DTask<StartRightNow> DBuzzer::PlayMelody_Task(const DMelody& InMelody, bool IsLooped)
{
	bIsMelodyPlayed = true;
	const uint Duration1_32 = (60000000 / (InMelody.Temp / 4)) / 32;

	do
	{
		for(const auto& Note : InMelody.Notes)
		{
			if(!bIsMelodyPlayed) { co_return; }

			PlayTone(Note.Tone);
			co_await DelayMicros(Duration1_32 * Note.Duration);
		}
	} while(IsLooped);

	Stop();
}


