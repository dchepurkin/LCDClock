#include "DPolyBuzzer.h"

void DPolyBuzzer::PlaySong(const DSong& InSong, bool IsLooped)
{
	PlaySong_Task(InSong, IsLooped);
}

void DPolyBuzzer::Stop()
{
	HighBuzzer.Stop();
	LowBuzzer.Stop();
}

DTask<StartRightNow> DPolyBuzzer::PlaySong_Task(const DSong& InSong, bool IsLooped)
{
	co_await DelayMillis(500);
	HighBuzzer.PlayMelody(InSong.HighMelody, IsLooped);
	LowBuzzer.PlayMelody(InSong.LowMelody, IsLooped);
}
