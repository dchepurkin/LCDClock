#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include <coroutine>
#include <pico/stdlib.h>
#include <algorithm>

struct StartRightNow
{
	auto get_return_object()
	{
		return std::coroutine_handle<StartRightNow>::from_promise(*this);
	}

	auto initial_suspend() noexcept { return std::suspend_never{}; }

	auto final_suspend() noexcept { return std::suspend_always{}; }

	void unhandled_exception() {}

	void return_void() noexcept {}

	[[nodiscard]] alarm_id_t GetAlarmID() const { return AlarmId; }

	void SetAlarmID(const alarm_id_t InAlarmID) { AlarmId = InAlarmID; }

private:
	alarm_id_t AlarmId = 0;
};

struct StartDeferred
{
	auto get_return_object()
	{
		return std::coroutine_handle<StartDeferred>::from_promise(*this);
	}

	auto initial_suspend() noexcept { return std::suspend_always{}; }

	auto final_suspend() noexcept { return std::suspend_always{}; }

	void unhandled_exception() {}

	void return_void() noexcept {}

	[[nodiscard]] alarm_id_t GetAlarmID() const { return AlarmId; }

	void SetAlarmID(const alarm_id_t InAlarmID) { AlarmId = InAlarmID; }

private:
	alarm_id_t AlarmId = 0;
};

template<typename PromiseType>
class DTask
{
public:
	using promise_type = PromiseType;
	using handle_type = std::coroutine_handle<promise_type>;

	DTask(handle_type InHandle) noexcept
			: Handle(InHandle) {}

	DTask(const DTask&) = delete;

	DTask(DTask&& Other) noexcept
			: Handle(Other.Handle)
	{
		Other.Handle = nullptr;
	}

	void Break()
	{
		if(const auto ID = GetAlarmID()) { cancel_alarm(ID); };
		if(Handle) { Handle.destroy(); }
	}

	void Start()
	{
		if(Handle) { Handle.resume(); }
	}

	/*struct promise_type
	{
		auto get_return_object()
		{
			return handle_type::from_promise(*this);
		}

		auto initial_suspend() noexcept
		{
			return std::suspend_never{};
		}

		auto final_suspend() noexcept
		{
			return std::suspend_always{};
		}

		void unhandled_exception() {}

		[[nodiscard]] alarm_id_t GetAlarmID() const { return AlarmId; }

		void SetAlarmID(const alarm_id_t InAlarmID) { AlarmId = InAlarmID; }

	private:
		alarm_id_t AlarmId = 0;
	};*/

private:
	handle_type Handle;

	[[nodiscard]] alarm_id_t GetAlarmID() const { return Handle.promise().GetAlarmID(); }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////

class DFuture
{
public:
	bool await_ready() const noexcept //���������� false - ������ ��� ���������� �� ���������� ����� � ����������� �������
	{
		return false;
	}

	template<typename PromiseType>
	void await_suspend(std::coroutine_handle<PromiseType> InHandle) noexcept
	{
		InHandle.promise().SetAlarmID(AlarmId);
		Handle = InHandle;
	}

	void await_resume() const noexcept {} //����� ���������� �������� ��� ������� co_await

	void SetReady() { Handle.resume(); }

	void SetAlarmID(const alarm_id_t InAlarmID) { AlarmId = InAlarmID; }

private:
	std::coroutine_handle<> Handle;

	alarm_id_t AlarmId = 0;
};

inline DFuture DelayMillis(uint32_t ms)
{
	DFuture Future;
	Future.SetAlarmID(add_alarm_in_ms(ms, [](alarm_id_t id, void* user_data) -> int64_t
	{
		static_cast<DFuture*>(user_data)->SetReady();
		return 0;
	}, &Future, false));

	return Future;
}

inline DFuture DelayMicros(uint32_t us)
{
	DFuture Future;
	Future.SetAlarmID(add_alarm_in_us(us, [](alarm_id_t id, void* user_data) -> int64_t
	{
		static_cast<DFuture*>(user_data)->SetReady();
		return 0;
	}, &Future, false));

	return Future;
}