#pragma once

class DVector2D
{
public:
	DVector2D(const int InX, const int InY)
			: X(InX),
			  Y(InY) {}

	DVector2D()
			: DVector2D(0, 0) {}

	constexpr bool operator==(const DVector2D& Other) const noexcept;
	constexpr bool operator!=(const DVector2D& Other) const noexcept;

	DVector2D operator+(const DVector2D& Other) const noexcept;
	DVector2D operator-(const DVector2D& Other) const noexcept;
	DVector2D operator*(const DVector2D& Other) const noexcept;
	DVector2D operator/(const DVector2D& Other) const noexcept;

	DVector2D operator*(const int InValue) const noexcept;
	DVector2D operator/(const int InValue) const noexcept;

	void Set(const int InX, const int InY) noexcept;

	int X;

	int Y;
};