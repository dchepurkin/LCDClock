#include "DVector2D.h"

constexpr bool DVector2D::operator==(const DVector2D& Other) const noexcept
{
	return X == Other.X && Y == Other.Y;
}

constexpr bool DVector2D::operator!=(const DVector2D& Other) const noexcept
{
	return X != Other.X || Y != Other.Y;
}

DVector2D DVector2D::operator+(const DVector2D& Other) const noexcept
{
	return DVector2D{X + Other.X, Y + Other.Y};
}

DVector2D DVector2D::operator-(const DVector2D& Other) const noexcept
{
	return DVector2D{X - Other.X, Y - Other.Y};
}

DVector2D DVector2D::operator*(const DVector2D& Other) const noexcept
{
	return DVector2D{X * Other.X, Y * Other.Y};
}

DVector2D DVector2D::operator/(const DVector2D& Other) const noexcept
{
	return DVector2D{Other.X == 0 ? 0 : X / Other.X, Other.Y == 0 ? 0 : Y / Other.Y};
}

DVector2D DVector2D::operator*(const int InValue) const noexcept
{
	return DVector2D{X * InValue, Y * InValue};
}

DVector2D DVector2D::operator/(const int InValue) const noexcept
{
	return InValue == 0 ? DVector2D{} : DVector2D{X / InValue, Y / InValue};
}

void DVector2D::Set(const int InX, const int InY) noexcept
{
	X = InX;
	Y = InY;
}
