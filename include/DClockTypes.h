#pragma once

#include <pico/stdlib.h>
#include <hardware/flash.h>
#include "DDelegate.h"
#include "DS1307Types.h"

#define ALARM_INFO_OFFSET (PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE)
#define ALARM_SONG_OFFSET (PICO_FLASH_SIZE_BYTES - (FLASH_SECTOR_SIZE * 2))

enum class ESwitchDirection : uint8_t
{
	Left,
	Right,
	Up,
	Down
};

enum class ESettingsState : uint8_t
{
	ChooseSettings,
	TimeSettings,
	AlarmSettings,
	WifiSettings,
	LightSettings
};

enum class ETimeSettingsState : uint8_t
{
	HourSetup,
	MinutesSetup,
	DaySetup,
	MonthSetup,
	YearSetup
};

enum class ENewAlarmState : uint8_t
{
	HourSetup,
	MinutesSetup,
	AlarmTypeSetup
};

enum class EAlarmSettingsState : uint8_t
{
	MelodySettings,
	AlarmTimeSettings,
	AddNewTime
};

enum class EClockState : uint8_t
{
	Idle,
	Alarm,
	Settings,
};

enum class EAlarmType : uint8_t
{
	EveryDay, //������ ����
	Once,    //���� ���
	Weekdays //�����
};

struct DAlarmInfo
{
	DTime Time;
	EAlarmType Type;
	bool bIsActive = true;
};

class DClockFunctionLibrary
{
public:
	DClockFunctionLibrary() = delete;

	DClockFunctionLibrary(const DClockFunctionLibrary&) = delete;

	static bool IsWeekday(const DDate& InDate) { return InDate.DayOfWeek != 6 && InDate.DayOfWeek != 7; }

	static std::string AlarmTypeToString(EAlarmType InType)
	{
		switch(InType)
		{
			case EAlarmType::EveryDay: return "���������";
			case EAlarmType::Once: return "���� ���";
			case EAlarmType::Weekdays: return "�� ������";
		}

		return {};
	}

	static void AddAlarmType(EAlarmType& InType, int8_t InValue)
	{
		auto TypeIndex = static_cast<int8_t>(InType) + InValue;
		if(TypeIndex < 0) { TypeIndex = 2; }
		else if(TypeIndex > 2) { TypeIndex = 0; }

		InType = static_cast<EAlarmType>(TypeIndex);
	}

	static std::string AlarmToString(const DAlarmInfo& InAlarm)
	{
		return std::to_string(InAlarm.Time.GetHighHourNum())
			   + std::to_string(InAlarm.Time.GetLowHourNum())
			   + " "
			   + std::to_string(InAlarm.Time.GetHighMinutesNum())
			   + std::to_string(InAlarm.Time.GetLowMinutesNum())
			   + " "
			   + AlarmTypeToString(InAlarm.Type);
	}
};