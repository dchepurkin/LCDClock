#pragma once

#include <pico/stdlib.h>
#include "ESP_01.h"
#include "DS1307.h"
#include "DCoroutine.h"
#include "DDelegate.h"

DECLARE_DELEGATE(OnSynchronizeSignature);

class DSynchronizer
{
public:
	OnSynchronizeSignature OnDateSynchronized;
	OnSynchronizeSignature OnTimeSynchronized;

	DSynchronizer() = delete;

	DSynchronizer(DDate& InDate, DTime& InTime);

	template<class UserClass>
	void BindOnWifiStatusChanged(UserClass* InObject, MethodOneParam<UserClass, bool> InCallback)
	{
		ESP.OnWifiConnected.Bind(InObject, InCallback);
	}

	void Synchronize();

	bool IsWifiConnected() const { return ESP.IsConnected(); }

	void Sleep()
	{
		bIsSleeping = true;
		ESP.SetSleepEnabled(true);
	}

	void Wake()
	{
		bIsSleeping = false;
		ESP.SetSleepEnabled(false);
	}

	void SetTime(const DTime& InTime) const;

	void SetDate(const DDate& InDate) const;

private:
	DDate& Date;

	DTime& Time;

	ESP_01 ESP{9, 8};

	DS1307 Ds1307;

	bool bIsSleeping = false;

	void WifiConnected_Callback(bool IsConnected);

	void GetDateTime_Callback(const DDate& InDate, const DTime& InTime);

	void Ds1307Synchronize();

	void WifiSyncFailed_Callback();

	DTask<StartRightNow> SyncDateTime_Task();

	DTask<StartRightNow> ESPInit_Task();

	DTask<StartRightNow> ResyncWifi();
};