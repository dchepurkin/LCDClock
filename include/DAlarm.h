#pragma once

#include <pico/stdlib.h>
#include "DClockTypes.h"
#include "DDelegate.h"

DECLARE_DELEGATE(OnAlarmTriggeredSignature);

class DAlarm
{
public:
	OnAlarmTriggeredSignature OnAlarmTriggered;

	DAlarm();

	void Check(const DDate& InDate, const DTime& InTime);

	//��������� ����� ���������, ���������� true ��� �������� ����������, false ���� ����� ��������� ��� ����������
	bool Add(const DAlarmInfo& InInfo);

	//���������� true ���� ���������� ���� �� ���� ���������
	bool IsActive() const;

	void Remove(uint8_t InIndex);

	//��������������� ������� ��� �������� ���� �����������
	void RemoveAll();

	//���������� ���������� ����������� �����������
	uint8_t GetAlarmAmount() const { return AlarmAmount; }

	//���������� ���������� � ����������� �����������
	DAlarmInfo* GetAlarmInfo() const { return AlarmTimes; }

	void ToggleAlarm(uint8_t InIndex);

private:
	uint8_t AlarmAmount = 0;

	DAlarmInfo* AlarmTimes;

	void ReadInfoFromFlash();
};