#pragma once

#include <vector>
#include "DClockTypes.h"
#include "DButton.h"
#include "DScreen.h"
#include "DCoroutine.h"
#include "DDelegate.h"
#include "DAlarm.h"
#include "DPolyBuzzer.h"
#include "DMelodies.h"
#include "DSynchronizer.h"
#include "Ds18b20.h"

class DClock
{
public:
	void Begin();

	void TempUpdateTick();

	//������������� ������� ��� ����������
	void SetSong(const DSong& InSong);

	void ShowTime(const DTime& InTime) const;

	void ShowDate(const DDate& InDate);

	void ShowDayOfWeek(const DDate& InDate);

	void DrawColon(bool IsColonOn);

private:
	EClockState State;

	Ds18b20 TempSensor{0};

	DTime Time;

	DDate Date;

	DTime SleepTime{22, 0};

	DTime WakeTime{7, 0};

	DSynchronizer Synchronizer{Date, Time};

	DButton TopButton{10};

	DAlarm Alarm;

	DPolyBuzzer Buzzer{12, 2};

	DScreen Screen;

	DSong Song{SuperMario};

	void TopButtonPressed_Callback();
	void TopButtonHoldDown_Callback();

	void AlarmTriggered_Callback();

	DTask<StartRightNow> TimeTick_Task();
	DTask<StartRightNow> ColonTick_Task();

	void DrawWifiIcon(bool IsConnected);
	void DrawAlarmIcon(bool IsActive);
	void FoundWifi_Callback(const std::list<std::string>& InWifiNames);
	void DateSynchronized_Callback();
	void TimeSynchronized_Callback();

	template<EClockState InState>
	void SetState();

	//settings
	uint8_t FocusedSongIndex = 1;

	DButton LeftButton{13};
	DButton CenterButton{14};
	DButton RightButton{15};
	DButton UpButton{28};
	DButton DownButton{29};

	ESettingsState SettingsState = ESettingsState::ChooseSettings;

	ESettingsState FocusedSetting = ESettingsState::TimeSettings;

	ETimeSettingsState TimeSettingsState = ETimeSettingsState::HourSetup;

	EAlarmSettingsState AlarmSettingsState = EAlarmSettingsState::MelodySettings;

	ENewAlarmState NewAlarmState = ENewAlarmState::HourSetup;

	DTime SetupTime;

	DDate SetupDate;

	DAlarmInfo NewAlarm;
	uint8_t AlarmsOffset = 0;
	uint8_t AlarmCursorPosition = 0;

	const std::vector<DSong> Songs = {Elochka, CrazyFrog, SuperMario};

	void LeftButtonPressed_Callback();
	void CenterButtonPressed_Callback();
	void RightButtonPressed_Callback();
	void UpButtonPressed_Callback();
	void DownButtonPressed_Callback();

	void UpButtonHoldDown_Callback();
	void DownButtonHoldDown_Callback();

	DTask<StartRightNow> TimeSetupBlinkTask();
	DTask<StartRightNow> NewAlarmBlinkTask();

	void SwitchFocusedSetting(ESwitchDirection InDirection);

	void SwitchFocusedSong(ESwitchDirection InDirection);

	void SwitchFocusedAlarm(ESwitchDirection InDirection);

	void SetAlarmSettingsState(EAlarmSettingsState InState);

	void ShowMelodySettings();

	void ShowAlarmTimeSettings();

	void ShowAddNewAlarmTimeScreen();

	void ShowAlarmType(EAlarmType InType);

	std::string GetSettingsTextName(ESettingsState InState);

	void ClearSettingsScreen() const;

	void ShowTemp();

	void DrawTempIcon();
};
