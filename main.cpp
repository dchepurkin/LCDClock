#ifdef DEBUG
	#include <cstdio>
#endif

#include <pico/stdlib.h>
#include "pico/multicore.h"
#include "DClock.h"

DClock Clock;

void Core1_Entry()
{
	Clock.TempUpdateTick();
}

int main()
{
#ifdef DEBUG
	//stdio_init_all();
#endif

	multicore_launch_core1(Core1_Entry);
	Clock.Begin();
}
